--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.6
-- Dumped by pg_dump version 9.6.6

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'SQL_ASCII';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = sbp_all, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: registered_merchants; Type: TABLE; Schema: sbp_all; Owner: palantir
--

CREATE TABLE registered_merchants (
    serial_id integer NOT NULL,
    platform_id integer NOT NULL,
    merchant_id bigint NOT NULL,
    registration_timestamp timestamp without time zone NOT NULL
);


ALTER TABLE registered_merchants OWNER TO palantir;

--
-- Name: TABLE registered_merchants; Type: COMMENT; Schema: sbp_all; Owner: palantir
--

COMMENT ON TABLE registered_merchants IS 'Merchants registered in this category.';


SET search_path = sbp_all_v0, pg_catalog;

--
-- Name: pipeline_merchants; Type: TABLE; Schema: sbp_all_v0; Owner: palantir
--

CREATE TABLE pipeline_merchants (
    serial_id integer NOT NULL,
    platform_id integer NOT NULL,
    merchant_id bigint NOT NULL,
    fletcher_checkpoint integer NOT NULL,
    clover_checkpoint integer NOT NULL,
    update_timestamp timestamp without time zone
);


ALTER TABLE pipeline_merchants OWNER TO palantir;

--
-- Name: metrics; Type: TABLE; Schema: sbp_all_v0; Owner: palantir
--

CREATE TABLE metrics (
    platform_id smallint NOT NULL,
    merchant_id bigint NOT NULL,
    time_window streaming.time_window NOT NULL,
    date date NOT NULL,
    customer_class streaming.customer_class NOT NULL,
    payment_count integer,
    revenue bigint,
    hourly_payment_count integer[],
    hourly_revenue bigint[],
    travel_distance_histogram integer[],
    visits_histogram integer[],
    revenue_by_payment_type jsonb,
    items_sales jsonb,
    recent_transactions json,
    customers_by_geohash jsonb
);


ALTER TABLE metrics OWNER TO palantir;

SET search_path = sbp_all, pg_catalog;

--
-- Name: registered_merchants_serial_id_seq; Type: SEQUENCE; Schema: sbp_all; Owner: palantir
--

CREATE SEQUENCE registered_merchants_serial_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE registered_merchants_serial_id_seq OWNER TO palantir;

--
-- Name: registered_merchants_serial_id_seq; Type: SEQUENCE OWNED BY; Schema: sbp_all; Owner: palantir
--

ALTER SEQUENCE registered_merchants_serial_id_seq OWNED BY registered_merchants.serial_id;


SET search_path = streaming, pg_catalog;

--
-- Name: categories; Type: TABLE; Schema: streaming; Owner: palantir
--

CREATE TABLE categories (
    id character varying(64) NOT NULL,
    CONSTRAINT categories_id_check CHECK (((id)::text ~ '^[a-z0-9_]+$'::text))
);


ALTER TABLE categories OWNER TO palantir;

--
-- Name: pipelines; Type: TABLE; Schema: streaming; Owner: palantir
--

CREATE TABLE pipelines (
    id character varying(64) NOT NULL,
    category_id character varying(64) NOT NULL,
    creation_timestamp timestamp without time zone NOT NULL,
    is_promoted boolean NOT NULL,
    CONSTRAINT pipelines_id_check CHECK (((id)::text ~ '^[a-z0-9_]+$'::text))
);


ALTER TABLE pipelines OWNER TO palantir;

SET search_path = sbp_all, pg_catalog;

--
-- Name: registered_merchants serial_id; Type: DEFAULT; Schema: sbp_all; Owner: palantir
--

ALTER TABLE ONLY registered_merchants ALTER COLUMN serial_id SET DEFAULT nextval('registered_merchants_serial_id_seq'::regclass);


--
-- Name: registered_merchants registered_merchants_pkey; Type: CONSTRAINT; Schema: sbp_all; Owner: palantir
--

ALTER TABLE ONLY registered_merchants
    ADD CONSTRAINT registered_merchants_pkey PRIMARY KEY (serial_id);


--
-- Name: registered_merchants unique_platform_id_merchant_id; Type: CONSTRAINT; Schema: sbp_all; Owner: palantir
--

ALTER TABLE ONLY registered_merchants
    ADD CONSTRAINT unique_platform_id_merchant_id UNIQUE (platform_id, merchant_id);


SET search_path = sbp_all_v0, pg_catalog;

--
-- Name: metrics metrics_pkey; Type: CONSTRAINT; Schema: sbp_all_v0; Owner: palantir
--

ALTER TABLE ONLY metrics
    ADD CONSTRAINT metrics_pkey PRIMARY KEY (platform_id, merchant_id, time_window, date, customer_class);


--
-- Name: pipeline_merchants pipeline_merchants_pkey; Type: CONSTRAINT; Schema: sbp_all_v0; Owner: palantir
--

ALTER TABLE ONLY pipeline_merchants
    ADD CONSTRAINT pipeline_merchants_pkey PRIMARY KEY (serial_id);


SET search_path = streaming, pg_catalog;

--
-- Name: categories categories_pkey; Type: CONSTRAINT; Schema: streaming; Owner: palantir
--

ALTER TABLE ONLY categories
    ADD CONSTRAINT categories_pkey PRIMARY KEY (id);


--
-- Name: pipelines pipelines_pkey; Type: CONSTRAINT; Schema: streaming; Owner: palantir
--

ALTER TABLE ONLY pipelines
    ADD CONSTRAINT pipelines_pkey PRIMARY KEY (id);


SET search_path = sbp_all, pg_catalog;

--
-- Name: registered_merchants_registration_timestamp_idx; Type: INDEX; Schema: sbp_all; Owner: palantir
--

CREATE INDEX registered_merchants_registration_timestamp_idx ON registered_merchants USING btree (registration_timestamp);


SET search_path = sbp_all_v0, pg_catalog;

--
-- Name: pipeline_merchants_platform_id_merchant_id_idx; Type: INDEX; Schema: sbp_all_v0; Owner: palantir
--

CREATE INDEX pipeline_merchants_platform_id_merchant_id_idx ON pipeline_merchants USING btree (platform_id, merchant_id);


--
-- Name: pipeline_merchants pipeline_merchants_serial_id_fkey; Type: FK CONSTRAINT; Schema: sbp_all_v0; Owner: palantir
--

ALTER TABLE ONLY pipeline_merchants
    ADD CONSTRAINT pipeline_merchants_serial_id_fkey FOREIGN KEY (serial_id) REFERENCES sbp_all.registered_merchants(serial_id);


SET search_path = streaming, pg_catalog;

--
-- Name: pipelines pipelines_category_id_fkey; Type: FK CONSTRAINT; Schema: streaming; Owner: palantir
--

ALTER TABLE ONLY pipelines
    ADD CONSTRAINT pipelines_category_id_fkey FOREIGN KEY (category_id) REFERENCES categories(id);


SET search_path = sbp_all, pg_catalog;

--
-- Name: registered_merchants; Type: ACL; Schema: sbp_all; Owner: palantir
--

GRANT SELECT ON TABLE registered_merchants TO sbp;


SET search_path = streaming, pg_catalog;

--
-- Name: categories; Type: ACL; Schema: streaming; Owner: palantir
--

GRANT SELECT ON TABLE categories TO sbp;


--
-- Name: pipelines; Type: ACL; Schema: streaming; Owner: palantir
--

GRANT SELECT ON TABLE pipelines TO sbp;


--
-- PostgreSQL database dump complete
--
