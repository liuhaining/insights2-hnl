--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.6
-- Dumped by pg_dump version 9.6.6

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner:
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner:
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: dblink; Type: EXTENSION; Schema: -; Owner:
--

CREATE EXTENSION IF NOT EXISTS dblink WITH SCHEMA public;


--
-- Name: EXTENSION dblink; Type: COMMENT; Schema: -; Owner:
--

COMMENT ON EXTENSION dblink IS 'connect to other PostgreSQL databases from within a database';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: account_confirmation; Type: TABLE; Schema: public; Owner: palantir
--

CREATE TABLE account_confirmation (
    user_id integer,
    link_hash character varying,
    valid_until timestamp without time zone,
    time_confirmed timestamp without time zone
);


ALTER TABLE account_confirmation OWNER TO palantir;

--
-- Name: accounts; Type: TABLE; Schema: public; Owner: palantir
--

CREATE TABLE accounts (
    key character varying,
    data character varying,
    user_id integer
);


ALTER TABLE accounts OWNER TO palantir;

--
-- Name: alerts; Type: TABLE; Schema: public; Owner: palantir
--

CREATE TABLE alerts (
    alert_id integer NOT NULL,
    alert_type character varying,
    store_id integer,
    data json,
    generation_time timestamp without time zone NOT NULL
);


ALTER TABLE alerts OWNER TO palantir;

--
-- Name: alerts_alert_id_seq; Type: SEQUENCE; Schema: public; Owner: palantir
--

CREATE SEQUENCE alerts_alert_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE alerts_alert_id_seq OWNER TO palantir;

--
-- Name: alerts_alert_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: palantir
--

ALTER SEQUENCE alerts_alert_id_seq OWNED BY alerts.alert_id;


--
-- Name: app_alerts; Type: TABLE; Schema: public; Owner: palantir
--

CREATE TABLE app_alerts (
    app_alert_id integer NOT NULL,
    alert_id integer,
    is_read boolean DEFAULT false
);


ALTER TABLE app_alerts OWNER TO palantir;

--
-- Name: app_alerts_app_alert_id_seq; Type: SEQUENCE; Schema: public; Owner: palantir
--

CREATE SEQUENCE app_alerts_app_alert_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE app_alerts_app_alert_id_seq OWNER TO palantir;

--
-- Name: app_alerts_app_alert_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: palantir
--

ALTER SEQUENCE app_alerts_app_alert_id_seq OWNED BY app_alerts.app_alert_id;


--
-- Name: app_events; Type: TABLE; Schema: public; Owner: palantir
--

CREATE TABLE app_events (
    user_id integer,
    username character varying,
    user_ip_addr character varying,
    session_id character varying,
    component character varying,
    action character varying,
    useragent_source character varying,
    device character varying,
    browser character varying,
    browser_version character varying,
    details character varying,
    user_ts timestamp without time zone,
    server_ts timestamp without time zone
);


ALTER TABLE app_events OWNER TO palantir;

--
-- Name: app_languages; Type: TABLE; Schema: public; Owner: palantir
--

CREATE TABLE app_languages (
    user_id integer,
    app_language character varying
);


ALTER TABLE app_languages OWNER TO palantir;

--
-- Name: auth; Type: TABLE; Schema: public; Owner: palantir
--

CREATE TABLE auth (
    password_hash character varying,
    salt character varying,
    user_id integer
);


ALTER TABLE auth OWNER TO palantir;

--
-- Name: bookmarks; Type: TABLE; Schema: public; Owner: palantir
--

CREATE TABLE bookmarks (
    store_id integer,
    type character varying,
    key character varying,
    note character varying,
    deleted boolean DEFAULT false,
    added_date date,
    deleted_date date,
    note_date date,
    metadata character varying,
    user_id integer,
    title character varying,
    id integer NOT NULL,
    added_datetime timestamp without time zone,
    note_datetime timestamp without time zone
);


ALTER TABLE bookmarks OWNER TO palantir;

--
-- Name: bookmarks_id_seq; Type: SEQUENCE; Schema: public; Owner: palantir
--

CREATE SEQUENCE bookmarks_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE bookmarks_id_seq OWNER TO palantir;

--
-- Name: bookmarks_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: palantir
--

ALTER SEQUENCE bookmarks_id_seq OWNED BY bookmarks.id;


--
-- Name: clover_app_install_statuses; Type: TABLE; Schema: public; Owner: palantir
--

CREATE TABLE clover_app_install_statuses (
    clover_merchant_id character varying NOT NULL,
    is_installed boolean NOT NULL,
    modified_at timestamp without time zone,
    created_at timestamp without time zone NOT NULL
);


ALTER TABLE clover_app_install_statuses OWNER TO palantir;

--
-- Name: clover_data_pipeline_status; Type: TABLE; Schema: public; Owner: palantir
--

CREATE TABLE clover_data_pipeline_status (
    store_id integer,
    pipeline_status character varying,
    user_id bigint,
    "timestamp" bigint,
    host_name character varying,
    association_id character varying
);


ALTER TABLE clover_data_pipeline_status OWNER TO palantir;

--
-- Name: clover_features; Type: TABLE; Schema: public; Owner: palantir
--

CREATE TABLE clover_features (
    store_id integer,
    new_feature character varying
);


ALTER TABLE clover_features OWNER TO palantir;

--
-- Name: clover_smart_auth; Type: TABLE; Schema: public; Owner: palantir
--

CREATE TABLE clover_smart_auth (
    log_id integer NOT NULL,
    user_id integer,
    store_id bigint,
    time_logged timestamp without time zone,
    clover_data json,
    auth_data json,
    request_data json,
    error_data json
);


ALTER TABLE clover_smart_auth OWNER TO palantir;

--
-- Name: clover_smart_auth_credentials; Type: TABLE; Schema: public; Owner: palantir
--

CREATE TABLE clover_smart_auth_credentials (
    clover_merchant_id character varying,
    clover_access_token character varying,
    last_updated timestamp without time zone
);


ALTER TABLE clover_smart_auth_credentials OWNER TO palantir;

--
-- Name: clover_smart_auth_errors; Type: TABLE; Schema: public; Owner: palantir
--

CREATE TABLE clover_smart_auth_errors (
    clover_smart_auth_error_id integer NOT NULL,
    clover_merchant_id character varying,
    request character varying,
    caller character varying,
    callee character varying,
    args character varying,
    result character varying,
    message character varying,
    time_logged timestamp without time zone
);


ALTER TABLE clover_smart_auth_errors OWNER TO palantir;

--
-- Name: clover_smart_auth_errors_clover_smart_auth_error_id_seq; Type: SEQUENCE; Schema: public; Owner: palantir
--

CREATE SEQUENCE clover_smart_auth_errors_clover_smart_auth_error_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE clover_smart_auth_errors_clover_smart_auth_error_id_seq OWNER TO palantir;

--
-- Name: clover_smart_auth_errors_clover_smart_auth_error_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: palantir
--

ALTER SEQUENCE clover_smart_auth_errors_clover_smart_auth_error_id_seq OWNED BY clover_smart_auth_errors.clover_smart_auth_error_id;


--
-- Name: clover_smart_auth_log_id_seq; Type: SEQUENCE; Schema: public; Owner: palantir
--

CREATE SEQUENCE clover_smart_auth_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE clover_smart_auth_log_id_seq OWNER TO palantir;

--
-- Name: clover_smart_auth_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: palantir
--

ALTER SEQUENCE clover_smart_auth_log_id_seq OWNED BY clover_smart_auth.log_id;


--
-- Name: cohorts; Type: TABLE; Schema: public; Owner: palantir
--

CREATE TABLE cohorts (
    store_id integer,
    name character varying,
    generation_method character varying,
    latitude numeric,
    longitude numeric,
    radius numeric,
    merchant_ids character varying,
    update_timestamp timestamp without time zone
);


ALTER TABLE cohorts OWNER TO palantir;

--
-- Name: confirm_email_address; Type: TABLE; Schema: public; Owner: palantir
--

CREATE TABLE confirm_email_address (
    token character varying,
    date_token_generated timestamp without time zone,
    email_address character varying,
    expiration_date timestamp without time zone,
    date_confirmed timestamp without time zone,
    expired boolean
);


ALTER TABLE confirm_email_address OWNER TO palantir;

--
-- Name: email_events; Type: TABLE; Schema: public; Owner: palantir
--

CREATE TABLE email_events (
    sg_event_id character varying,
    sg_message_id character varying,
    event character varying,
    email character varying,
    sbp_email_type character varying,
    "timestamp" character varying,
    useragent character varying,
    url character varying,
    full_event text
);


ALTER TABLE email_events OWNER TO palantir;

--
-- Name: email_registration; Type: TABLE; Schema: public; Owner: palantir
--

CREATE TABLE email_registration (
    email character varying,
    hash character varying,
    created timestamp without time zone
);


ALTER TABLE email_registration OWNER TO palantir;

--
-- Name: email_templates; Type: TABLE; Schema: public; Owner: palantir
--

CREATE TABLE email_templates (
    name character varying,
    variant character varying,
    variant_version integer,
    template text,
    subject character varying,
    less text,
    description character varying,
    enabled boolean,
    ts timestamp without time zone
);


ALTER TABLE email_templates OWNER TO palantir;

--
-- Name: empathy; Type: TABLE; Schema: public; Owner: palantir
--

CREATE TABLE empathy (
    logtime timestamp without time zone,
    userid integer,
    page character varying,
    id character varying,
    dt integer,
    storeid integer,
    metadata character varying
);


ALTER TABLE empathy OWNER TO palantir;

--
-- Name: error_reports; Type: TABLE; Schema: public; Owner: palantir
--

CREATE TABLE error_reports (
    error_uuid uuid,
    error_name character varying,
    error_message character varying,
    error_level character varying,
    error_stack text,
    error_time timestamp without time zone DEFAULT now(),
    user_id integer,
    store_id integer,
    metadata json DEFAULT '{}'::json
);


ALTER TABLE error_reports OWNER TO palantir;

--
-- Name: feedbacks; Type: TABLE; Schema: public; Owner: palantir
--

CREATE TABLE feedbacks (
    user_id integer,
    date_submitted timestamp without time zone,
    app_version character varying,
    url character varying,
    feedback character varying
);


ALTER TABLE feedbacks OWNER TO palantir;

--
-- Name: stores; Type: TABLE; Schema: public; Owner: palantir
--

CREATE TABLE stores (
    store_mid character varying,
    store_name character varying,
    base_filters character varying,
    comp_filters character varying,
    store_address character varying,
    backfill_status character varying,
    store_zipcode character varying,
    start_of_day_offset integer,
    stream_status character varying,
    comp_filter_generation_method character varying,
    comp_filter_created_date bigint,
    store_icon_name character varying,
    factual_taxonomy_id integer,
    price_category integer,
    factual_interested_neighborhood character varying,
    store_id integer NOT NULL,
    program_indicator boolean,
    trial_start_date bigint,
    store_init_status character varying,
    creation_ts bigint,
    program_indicator_enabled_ts bigint,
    clover_merchant_id character varying,
    clover_auth_token character varying,
    backfill_status_last_update_ts bigint DEFAULT '1420768347974'::bigint,
    is_clover_enabled boolean DEFAULT false,
    latitude numeric,
    longitude numeric,
    location_mode character varying,
    map_zoom integer,
    switch_date bigint,
    switch_date_override boolean DEFAULT false,
    is_prereg boolean,
    timezone_name character varying,
    clover_plan_name character varying,
    paywall_first_login_ts bigint,
    paywall_will_upgrade_ts bigint
);


ALTER TABLE stores OWNER TO palantir;

--
-- Name: user_ids; Type: TABLE; Schema: public; Owner: palantir
--

CREATE TABLE user_ids (
    user_id integer NOT NULL,
    username character varying,
    user_type character varying NOT NULL
);


ALTER TABLE user_ids OWNER TO palantir;

--
-- Name: user_store_mappings; Type: TABLE; Schema: public; Owner: palantir
--

CREATE TABLE user_store_mappings (
    store_id bigint,
    user_id integer,
    user_level character varying
);


ALTER TABLE user_store_mappings OWNER TO palantir;

--
-- Name: full_user_store_mappings_vw; Type: VIEW; Schema: public; Owner: palantir
--

CREATE VIEW full_user_store_mappings_vw AS
 SELECT a.user_id,
    a.username,
    a.user_type,
    b.user_level,
    c.store_id,
    c.store_mid,
    c.is_prereg,
    c.clover_merchant_id
   FROM ((user_ids a
     JOIN user_store_mappings b ON ((a.user_id = b.user_id)))
     JOIN stores c ON ((b.store_id = c.store_id)));


ALTER TABLE full_user_store_mappings_vw OWNER TO palantir;

--
-- Name: goals_global; Type: TABLE; Schema: public; Owner: palantir
--

CREATE TABLE goals_global (
    store_id integer,
    type character varying,
    json_data character varying
);


ALTER TABLE goals_global OWNER TO palantir;

--
-- Name: job_leases; Type: TABLE; Schema: public; Owner: palantir
--

CREATE TABLE job_leases (
    is_lease boolean,
    expiration timestamp without time zone,
    id character varying(40),
    namespace character varying(40)
);


ALTER TABLE job_leases OWNER TO palantir;

--
-- Name: key_values; Type: TABLE; Schema: public; Owner: palantir
--

CREATE TABLE key_values (
    key text NOT NULL,
    value text
);


ALTER TABLE key_values OWNER TO palantir;

--
-- Name: locations; Type: TABLE; Schema: public; Owner: palantir
--

CREATE TABLE locations (
    id integer,
    data character varying,
    keywords character varying,
    latitude real,
    longitude real
);


ALTER TABLE locations OWNER TO palantir;

--
-- Name: mobile_devices; Type: TABLE; Schema: public; Owner: palantir
--

CREATE TABLE mobile_devices (
    id integer NOT NULL,
    user_id integer,
    uuid character varying,
    ios_device_token character varying,
    ios_token_active boolean DEFAULT false,
    ios_token_marked_inactive_at timestamp without time zone,
    ios_token_marked_active_at timestamp without time zone,
    created_at timestamp without time zone,
    last_seen_at timestamp without time zone
);


ALTER TABLE mobile_devices OWNER TO palantir;

--
-- Name: mobile_devices_id_seq; Type: SEQUENCE; Schema: public; Owner: palantir
--

CREATE SEQUENCE mobile_devices_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE mobile_devices_id_seq OWNER TO palantir;

--
-- Name: mobile_devices_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: palantir
--

ALTER SEQUENCE mobile_devices_id_seq OWNED BY mobile_devices.id;


--
-- Name: notification_events; Type: TABLE; Schema: public; Owner: palantir
--

CREATE TABLE notification_events (
    event_id integer,
    notification_id integer,
    notification_type character varying,
    store_id integer,
    user_id integer,
    event_type character varying,
    event_timestamp timestamp without time zone
);


ALTER TABLE notification_events OWNER TO palantir;

--
-- Name: notification_prefs; Type: TABLE; Schema: public; Owner: palantir
--

CREATE TABLE notification_prefs (
    user_id integer,
    notification_type character varying,
    is_enabled boolean
);


ALTER TABLE notification_prefs OWNER TO palantir;

--
-- Name: notifications; Type: TABLE; Schema: public; Owner: palantir
--

CREATE TABLE notifications (
    notification_type character varying,
    user_id integer,
    store_id integer,
    data character varying,
    generation_time timestamp without time zone,
    notification_id integer NOT NULL
);


ALTER TABLE notifications OWNER TO palantir;

--
-- Name: notifications_notification_id_seq; Type: SEQUENCE; Schema: public; Owner: palantir
--

CREATE SEQUENCE notifications_notification_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE notifications_notification_id_seq OWNER TO palantir;

--
-- Name: notifications_notification_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: palantir
--

ALTER SEQUENCE notifications_notification_id_seq OWNED BY notifications.notification_id;


--
-- Name: notifications_v2; Type: TABLE; Schema: public; Owner: palantir
--

CREATE TABLE notifications_v2 (
    notification_id integer NOT NULL,
    alert_id integer,
    user_id integer,
    data json,
    send_time timestamp without time zone
);


ALTER TABLE notifications_v2 OWNER TO palantir;

--
-- Name: notifications_v2_notification_id_seq; Type: SEQUENCE; Schema: public; Owner: palantir
--

CREATE SEQUENCE notifications_v2_notification_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE notifications_v2_notification_id_seq OWNER TO palantir;

--
-- Name: notifications_v2_notification_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: palantir
--

ALTER SEQUENCE notifications_v2_notification_id_seq OWNED BY notifications_v2.notification_id;


--
-- Name: notifications_v3; Type: TABLE; Schema: public; Owner: palantir
--

CREATE TABLE notifications_v3 (
    mid character varying(40),
    notification_type character varying(10),
    best_date date
);


ALTER TABLE notifications_v3 OWNER TO palantir;

--
-- Name: objects; Type: TABLE; Schema: public; Owner: palantir
--

CREATE TABLE objects (
    hash character varying,
    type character varying,
    value character varying
);


ALTER TABLE objects OWNER TO palantir;

--
-- Name: sbp_auth; Type: TABLE; Schema: public; Owner: palantir
--

CREATE TABLE sbp_auth (
    password_hash character varying,
    salt character varying,
    password_expiration bigint,
    last_expiration_warning bigint,
    first_time_user bigint,
    failed_attempts integer,
    locked_out_until bigint,
    user_id integer
);


ALTER TABLE sbp_auth OWNER TO palantir;

--
-- Name: sbp_demo_auth; Type: TABLE; Schema: public; Owner: palantir
--

CREATE TABLE sbp_demo_auth (
    email_address character varying,
    demo_token character varying,
    time_generated character varying,
    time_visited character varying,
    expired boolean,
    mid character varying
);


ALTER TABLE sbp_demo_auth OWNER TO palantir;

--
-- Name: sbp_opened_email_links; Type: TABLE; Schema: public; Owner: palantir
--

CREATE TABLE sbp_opened_email_links (
    email_id integer,
    date_opened timestamp without time zone,
    redirect_url character varying
);


ALTER TABLE sbp_opened_email_links OWNER TO palantir;

--
-- Name: sbp_password_log; Type: TABLE; Schema: public; Owner: palantir
--

CREATE TABLE sbp_password_log (
    password_hash character varying,
    salt character varying,
    last_used bigint,
    user_id integer
);


ALTER TABLE sbp_password_log OWNER TO palantir;

--
-- Name: sbp_password_reset; Type: TABLE; Schema: public; Owner: palantir
--

CREATE TABLE sbp_password_reset (
    user_id integer,
    link_hash character varying,
    valid_until bigint
);


ALTER TABLE sbp_password_reset OWNER TO palantir;

--
-- Name: secrets; Type: TABLE; Schema: public; Owner: palantir
--

CREATE TABLE secrets (
    id character varying,
    key character varying,
    secret character varying,
    is_lock boolean,
    valid_until bigint
);


ALTER TABLE secrets OWNER TO palantir;

--
-- Name: sent_emails; Type: TABLE; Schema: public; Owner: palantir
--

CREATE TABLE sent_emails (
    email_address character varying,
    email_type character varying,
    date_sent timestamp without time zone,
    mid character varying,
    app_version character varying,
    email_id integer NOT NULL,
    email_secret character varying,
    delivered boolean,
    is_internal_email boolean,
    email_subject character varying DEFAULT 'unknown'::character varying,
    email_from character varying DEFAULT 'unknown'::character varying,
    email_recipient_overrides character varying DEFAULT 'unknown'::character varying,
    email_subject_override character varying DEFAULT 'unknown'::character varying,
    email_from_override character varying DEFAULT 'unknown'::character varying,
    email_tracking_override character varying DEFAULT 'unknown'::character varying,
    used_litmus boolean
);


ALTER TABLE sent_emails OWNER TO palantir;

--
-- Name: sent_emails_email_id_seq; Type: SEQUENCE; Schema: public; Owner: palantir
--

CREATE SEQUENCE sent_emails_email_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sent_emails_email_id_seq OWNER TO palantir;

--
-- Name: sent_emails_email_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: palantir
--

ALTER SEQUENCE sent_emails_email_id_seq OWNED BY sent_emails.email_id;


--
-- Name: sent_emails_prereg; Type: TABLE; Schema: public; Owner: palantir
--

CREATE TABLE sent_emails_prereg (
    app_version character varying,
    date_sent timestamp without time zone,
    delivered boolean,
    email_address character varying,
    email_subject character varying,
    email_from character varying,
    email_recipient_overrides character varying,
    email_subject_override character varying,
    email_from_override character varying,
    email_id integer,
    email_secret character varying,
    email_type character varying,
    email_tracking_override character varying,
    is_internal_email boolean,
    used_litmus boolean,
    mid character varying
);


ALTER TABLE sent_emails_prereg OWNER TO palantir;

--
-- Name: social_media; Type: TABLE; Schema: public; Owner: palantir
--

CREATE TABLE social_media (
    user_id integer,
    key character varying,
    data character varying
);


ALTER TABLE social_media OWNER TO palantir;

--
-- Name: stored_filters; Type: TABLE; Schema: public; Owner: palantir
--

CREATE TABLE stored_filters (
    key character varying,
    serialized_filter character varying
);


ALTER TABLE stored_filters OWNER TO palantir;

--
-- Name: stored_filters_temp; Type: TABLE; Schema: public; Owner: palantir
--

CREATE TABLE stored_filters_temp (
    key character varying,
    serialized_filter character varying
);


ALTER TABLE stored_filters_temp OWNER TO palantir;

--
-- Name: stores_store_id_seq; Type: SEQUENCE; Schema: public; Owner: palantir
--

CREATE SEQUENCE stores_store_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE stores_store_id_seq OWNER TO palantir;

--
-- Name: stores_store_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: palantir
--

ALTER SEQUENCE stores_store_id_seq OWNED BY stores.store_id;


--
-- Name: stores_temp; Type: TABLE; Schema: public; Owner: palantir
--

CREATE TABLE stores_temp (
    store_mid character varying,
    store_name character varying,
    base_filters character varying,
    local_filters character varying,
    comp_filters character varying,
    map_location character varying,
    store_address character varying,
    streaming_mode integer,
    backfill_status integer,
    store_zipcode character varying,
    start_of_day_offset integer,
    stream_status integer,
    comp_filter_generation_method character varying,
    comp_filter_created_date bigint,
    store_icon_name character varying,
    factual_taxonomy_id integer,
    price_category integer,
    factual_interested_neighborhood character varying,
    store_id integer
);


ALTER TABLE stores_temp OWNER TO palantir;

--
-- Name: stories; Type: TABLE; Schema: public; Owner: palantir
--

CREATE TABLE stories (
    id integer NOT NULL,
    story_date date,
    data character varying,
    normalized_target_date date,
    generation_date timestamp without time zone,
    type character varying,
    sentiment character varying,
    is_recalled boolean,
    last_refreshed_timestamp timestamp without time zone,
    store_id integer,
    product_version character varying DEFAULT 'unknown'::character varying,
    datasource character varying
);


ALTER TABLE stories OWNER TO palantir;

--
-- Name: stories_dev; Type: TABLE; Schema: public; Owner: palantir
--

CREATE TABLE stories_dev (
    id integer,
    story_date date,
    data text,
    normalized_target_date date,
    generation_date date,
    type text,
    sentiment text,
    is_recalled text,
    last_refreshed_timestamp text,
    store_id text,
    product_version text,
    datasource text
);


ALTER TABLE stories_dev OWNER TO palantir;

--
-- Name: stories_id_seq; Type: SEQUENCE; Schema: public; Owner: palantir
--

CREATE SEQUENCE stories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE stories_id_seq OWNER TO palantir;

--
-- Name: stories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: palantir
--

ALTER SEQUENCE stories_id_seq OWNED BY stories.id;


--
-- Name: story_events; Type: TABLE; Schema: public; Owner: palantir
--

CREATE TABLE story_events (
    story_id integer,
    user_id integer,
    event_type character varying,
    event_date timestamp without time zone,
    metadata json DEFAULT '{}'::json
);


ALTER TABLE story_events OWNER TO palantir;

--
-- Name: system_properties; Type: TABLE; Schema: public; Owner: palantir
--

CREATE TABLE system_properties (
    key character varying NOT NULL,
    value character varying
);


ALTER TABLE system_properties OWNER TO palantir;

--
-- Name: unsent_emails; Type: TABLE; Schema: public; Owner: palantir
--

CREATE TABLE unsent_emails (
    app_version character varying,
    date_sent timestamp without time zone,
    delivered boolean,
    reason_for_failure character varying,
    email_address character varying,
    email_subject character varying,
    email_from character varying,
    email_recipient_overrides character varying,
    email_subject_override character varying,
    email_from_override character varying,
    email_id integer NOT NULL,
    email_secret character varying,
    email_type character varying,
    email_tracking_override character varying,
    is_internal_email boolean,
    used_litmus boolean,
    mid character varying
);


ALTER TABLE unsent_emails OWNER TO palantir;

--
-- Name: unsent_emails_email_id_seq; Type: SEQUENCE; Schema: public; Owner: palantir
--

CREATE SEQUENCE unsent_emails_email_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE unsent_emails_email_id_seq OWNER TO palantir;

--
-- Name: unsent_emails_email_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: palantir
--

ALTER SEQUENCE unsent_emails_email_id_seq OWNED BY unsent_emails.email_id;


--
-- Name: unsent_emails_prereg; Type: TABLE; Schema: public; Owner: palantir
--

CREATE TABLE unsent_emails_prereg (
    app_version character varying,
    date_sent timestamp without time zone,
    delivered boolean,
    reason_for_failure character varying,
    email_address character varying,
    email_subject character varying,
    email_from character varying,
    email_recipient_overrides character varying,
    email_subject_override character varying,
    email_from_override character varying,
    email_id integer,
    email_secret character varying,
    email_type character varying,
    email_tracking_override character varying,
    is_internal_email boolean,
    used_litmus boolean,
    mid character varying
);


ALTER TABLE unsent_emails_prereg OWNER TO palantir;

--
-- Name: user_cohort_mappings; Type: TABLE; Schema: public; Owner: palantir
--

CREATE TABLE user_cohort_mappings (
    cohort_id integer,
    user_id integer
);


ALTER TABLE user_cohort_mappings OWNER TO palantir;

--
-- Name: user_cohorts; Type: TABLE; Schema: public; Owner: palantir
--

CREATE TABLE user_cohorts (
    cohort_id integer NOT NULL,
    name character varying,
    generation_method character varying,
    latitude numeric,
    longitude numeric,
    radius numeric,
    category_ids numeric[],
    merchant_ids character varying[],
    update_timestamp timestamp without time zone,
    create_timestamp timestamp without time zone,
    metadata json
);


ALTER TABLE user_cohorts OWNER TO palantir;

--
-- Name: user_cohorts_cohort_id_seq; Type: SEQUENCE; Schema: public; Owner: palantir
--

CREATE SEQUENCE user_cohorts_cohort_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE user_cohorts_cohort_id_seq OWNER TO palantir;

--
-- Name: user_cohorts_cohort_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: palantir
--

ALTER SEQUENCE user_cohorts_cohort_id_seq OWNED BY user_cohorts.cohort_id;


--
-- Name: user_ids_user_id_seq; Type: SEQUENCE; Schema: public; Owner: palantir
--

CREATE SEQUENCE user_ids_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE user_ids_user_id_seq OWNER TO palantir;

--
-- Name: user_ids_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: palantir
--

ALTER SEQUENCE user_ids_user_id_seq OWNED BY user_ids.user_id;


--
-- Name: user_indexed_cohorts; Type: TABLE; Schema: public; Owner: palantir
--

CREATE TABLE user_indexed_cohorts (
    user_id integer,
    indexed_cohort_id character varying,
    create_timestamp timestamp without time zone,
    metadata json
);


ALTER TABLE user_indexed_cohorts OWNER TO palantir;

--
-- Name: user_roles; Type: TABLE; Schema: public; Owner: palantir
--

CREATE TABLE user_roles (
    role_name character varying,
    user_id integer
);


ALTER TABLE user_roles OWNER TO palantir;

--
-- Name: x; Type: TABLE; Schema: public; Owner: palantir
--

CREATE TABLE x (
    store_id integer
);


ALTER TABLE x OWNER TO palantir;

--
-- Name: alerts alert_id; Type: DEFAULT; Schema: public; Owner: palantir
--

ALTER TABLE ONLY alerts ALTER COLUMN alert_id SET DEFAULT nextval('alerts_alert_id_seq'::regclass);


--
-- Name: app_alerts app_alert_id; Type: DEFAULT; Schema: public; Owner: palantir
--

ALTER TABLE ONLY app_alerts ALTER COLUMN app_alert_id SET DEFAULT nextval('app_alerts_app_alert_id_seq'::regclass);


--
-- Name: bookmarks id; Type: DEFAULT; Schema: public; Owner: palantir
--

ALTER TABLE ONLY bookmarks ALTER COLUMN id SET DEFAULT nextval('bookmarks_id_seq'::regclass);


--
-- Name: clover_smart_auth log_id; Type: DEFAULT; Schema: public; Owner: palantir
--

ALTER TABLE ONLY clover_smart_auth ALTER COLUMN log_id SET DEFAULT nextval('clover_smart_auth_log_id_seq'::regclass);


--
-- Name: clover_smart_auth_errors clover_smart_auth_error_id; Type: DEFAULT; Schema: public; Owner: palantir
--

ALTER TABLE ONLY clover_smart_auth_errors ALTER COLUMN clover_smart_auth_error_id SET DEFAULT nextval('clover_smart_auth_errors_clover_smart_auth_error_id_seq'::regclass);


--
-- Name: mobile_devices id; Type: DEFAULT; Schema: public; Owner: palantir
--

ALTER TABLE ONLY mobile_devices ALTER COLUMN id SET DEFAULT nextval('mobile_devices_id_seq'::regclass);


--
-- Name: notifications notification_id; Type: DEFAULT; Schema: public; Owner: palantir
--

ALTER TABLE ONLY notifications ALTER COLUMN notification_id SET DEFAULT nextval('notifications_notification_id_seq'::regclass);


--
-- Name: notifications_v2 notification_id; Type: DEFAULT; Schema: public; Owner: palantir
--

ALTER TABLE ONLY notifications_v2 ALTER COLUMN notification_id SET DEFAULT nextval('notifications_v2_notification_id_seq'::regclass);


--
-- Name: sent_emails email_id; Type: DEFAULT; Schema: public; Owner: palantir
--

ALTER TABLE ONLY sent_emails ALTER COLUMN email_id SET DEFAULT nextval('sent_emails_email_id_seq'::regclass);


--
-- Name: stores store_id; Type: DEFAULT; Schema: public; Owner: palantir
--

ALTER TABLE ONLY stores ALTER COLUMN store_id SET DEFAULT nextval('stores_store_id_seq'::regclass);


--
-- Name: stories id; Type: DEFAULT; Schema: public; Owner: palantir
--

ALTER TABLE ONLY stories ALTER COLUMN id SET DEFAULT nextval('stories_id_seq'::regclass);


--
-- Name: unsent_emails email_id; Type: DEFAULT; Schema: public; Owner: palantir
--

ALTER TABLE ONLY unsent_emails ALTER COLUMN email_id SET DEFAULT nextval('unsent_emails_email_id_seq'::regclass);


--
-- Name: user_cohorts cohort_id; Type: DEFAULT; Schema: public; Owner: palantir
--

ALTER TABLE ONLY user_cohorts ALTER COLUMN cohort_id SET DEFAULT nextval('user_cohorts_cohort_id_seq'::regclass);


--
-- Name: user_ids user_id; Type: DEFAULT; Schema: public; Owner: palantir
--

ALTER TABLE ONLY user_ids ALTER COLUMN user_id SET DEFAULT nextval('user_ids_user_id_seq'::regclass);


--
-- Name: alerts alerts_pkey; Type: CONSTRAINT; Schema: public; Owner: palantir
--

ALTER TABLE ONLY alerts
    ADD CONSTRAINT alerts_pkey PRIMARY KEY (alert_id);


--
-- Name: app_alerts app_alerts_alert_id_key; Type: CONSTRAINT; Schema: public; Owner: palantir
--

ALTER TABLE ONLY app_alerts
    ADD CONSTRAINT app_alerts_alert_id_key UNIQUE (alert_id);


--
-- Name: app_alerts app_alerts_pkey; Type: CONSTRAINT; Schema: public; Owner: palantir
--

ALTER TABLE ONLY app_alerts
    ADD CONSTRAINT app_alerts_pkey PRIMARY KEY (app_alert_id);


--
-- Name: auth authuniqueuserid; Type: CONSTRAINT; Schema: public; Owner: palantir
--

ALTER TABLE ONLY auth
    ADD CONSTRAINT authuniqueuserid UNIQUE (user_id);


--
-- Name: bookmarks bookmarks_pkey; Type: CONSTRAINT; Schema: public; Owner: palantir
--

ALTER TABLE ONLY bookmarks
    ADD CONSTRAINT bookmarks_pkey PRIMARY KEY (id);


--
-- Name: clover_smart_auth_errors clover_smart_auth_errors_pkey; Type: CONSTRAINT; Schema: public; Owner: palantir
--

ALTER TABLE ONLY clover_smart_auth_errors
    ADD CONSTRAINT clover_smart_auth_errors_pkey PRIMARY KEY (clover_smart_auth_error_id);


--
-- Name: clover_smart_auth clover_smart_auth_pkey; Type: CONSTRAINT; Schema: public; Owner: palantir
--

ALTER TABLE ONLY clover_smart_auth
    ADD CONSTRAINT clover_smart_auth_pkey PRIMARY KEY (log_id);


--
-- Name: account_confirmation emailconfirmationuniquehash; Type: CONSTRAINT; Schema: public; Owner: palantir
--

ALTER TABLE ONLY account_confirmation
    ADD CONSTRAINT emailconfirmationuniquehash UNIQUE (link_hash);


--
-- Name: account_confirmation emailconfirmationuniqueuserid; Type: CONSTRAINT; Schema: public; Owner: palantir
--

ALTER TABLE ONLY account_confirmation
    ADD CONSTRAINT emailconfirmationuniqueuserid UNIQUE (user_id);


--
-- Name: email_registration emailregistrationuniqueemail; Type: CONSTRAINT; Schema: public; Owner: palantir
--

ALTER TABLE ONLY email_registration
    ADD CONSTRAINT emailregistrationuniqueemail UNIQUE (email);


--
-- Name: email_registration emailregistrationuniquehash; Type: CONSTRAINT; Schema: public; Owner: palantir
--

ALTER TABLE ONLY email_registration
    ADD CONSTRAINT emailregistrationuniquehash UNIQUE (hash);


--
-- Name: key_values key_values_pkey; Type: CONSTRAINT; Schema: public; Owner: palantir
--

ALTER TABLE ONLY key_values
    ADD CONSTRAINT key_values_pkey PRIMARY KEY (key);


--
-- Name: locations locations_id_key; Type: CONSTRAINT; Schema: public; Owner: palantir
--

ALTER TABLE ONLY locations
    ADD CONSTRAINT locations_id_key UNIQUE (id);


--
-- Name: mobile_devices mobile_devices_pkey; Type: CONSTRAINT; Schema: public; Owner: palantir
--

ALTER TABLE ONLY mobile_devices
    ADD CONSTRAINT mobile_devices_pkey PRIMARY KEY (id);


--
-- Name: notifications notifications_pkey; Type: CONSTRAINT; Schema: public; Owner: palantir
--

ALTER TABLE ONLY notifications
    ADD CONSTRAINT notifications_pkey PRIMARY KEY (notification_id);


--
-- Name: notifications_v2 notifications_v2_pkey; Type: CONSTRAINT; Schema: public; Owner: palantir
--

ALTER TABLE ONLY notifications_v2
    ADD CONSTRAINT notifications_v2_pkey PRIMARY KEY (notification_id);


--
-- Name: objects objects_hash_key; Type: CONSTRAINT; Schema: public; Owner: palantir
--

ALTER TABLE ONLY objects
    ADD CONSTRAINT objects_hash_key UNIQUE (hash);


--
-- Name: sbp_auth sbpauthuniqueuserid; Type: CONSTRAINT; Schema: public; Owner: palantir
--

ALTER TABLE ONLY sbp_auth
    ADD CONSTRAINT sbpauthuniqueuserid UNIQUE (user_id);


--
-- Name: sbp_password_reset sbppasswordresetuniquehash; Type: CONSTRAINT; Schema: public; Owner: palantir
--

ALTER TABLE ONLY sbp_password_reset
    ADD CONSTRAINT sbppasswordresetuniquehash UNIQUE (link_hash);


--
-- Name: sbp_password_reset sbppasswordresetuniqueuserid; Type: CONSTRAINT; Schema: public; Owner: palantir
--

ALTER TABLE ONLY sbp_password_reset
    ADD CONSTRAINT sbppasswordresetuniqueuserid UNIQUE (user_id);


--
-- Name: cohorts sbpuniquecohort; Type: CONSTRAINT; Schema: public; Owner: palantir
--

ALTER TABLE ONLY cohorts
    ADD CONSTRAINT sbpuniquecohort UNIQUE (store_id);


--
-- Name: user_indexed_cohorts sbpuniqueindexedcohort; Type: CONSTRAINT; Schema: public; Owner: palantir
--

ALTER TABLE ONLY user_indexed_cohorts
    ADD CONSTRAINT sbpuniqueindexedcohort UNIQUE (user_id, indexed_cohort_id);


--
-- Name: sent_emails sent_emails_pkey; Type: CONSTRAINT; Schema: public; Owner: palantir
--

ALTER TABLE ONLY sent_emails
    ADD CONSTRAINT sent_emails_pkey PRIMARY KEY (email_id);


--
-- Name: social_media socialuniqueuserkey; Type: CONSTRAINT; Schema: public; Owner: palantir
--

ALTER TABLE ONLY social_media
    ADD CONSTRAINT socialuniqueuserkey UNIQUE (user_id, key);


--
-- Name: stores stores_pkey; Type: CONSTRAINT; Schema: public; Owner: palantir
--

ALTER TABLE ONLY stores
    ADD CONSTRAINT stores_pkey PRIMARY KEY (store_id);


--
-- Name: stories stories_pkey; Type: CONSTRAINT; Schema: public; Owner: palantir
--

ALTER TABLE ONLY stories
    ADD CONSTRAINT stories_pkey PRIMARY KEY (id);


--
-- Name: system_properties system_properties_pkey; Type: CONSTRAINT; Schema: public; Owner: palantir
--

ALTER TABLE ONLY system_properties
    ADD CONSTRAINT system_properties_pkey PRIMARY KEY (key);


--
-- Name: sent_emails unique_email_id; Type: CONSTRAINT; Schema: public; Owner: palantir
--

ALTER TABLE ONLY sent_emails
    ADD CONSTRAINT unique_email_id UNIQUE (email_id);


--
-- Name: stored_filters unique_key; Type: CONSTRAINT; Schema: public; Owner: palantir
--

ALTER TABLE ONLY stored_filters
    ADD CONSTRAINT unique_key UNIQUE (key);


--
-- Name: email_templates unique_name_variant_version; Type: CONSTRAINT; Schema: public; Owner: palantir
--

ALTER TABLE ONLY email_templates
    ADD CONSTRAINT unique_name_variant_version UNIQUE (name, variant, variant_version);


--
-- Name: clover_data_pipeline_status unique_store_id; Type: CONSTRAINT; Schema: public; Owner: palantir
--

ALTER TABLE ONLY clover_data_pipeline_status
    ADD CONSTRAINT unique_store_id UNIQUE (store_id);


--
-- Name: clover_smart_auth_credentials uniqueclovermerchantid; Type: CONSTRAINT; Schema: public; Owner: palantir
--

ALTER TABLE ONLY clover_smart_auth_credentials
    ADD CONSTRAINT uniqueclovermerchantid UNIQUE (clover_merchant_id);


--
-- Name: sbp_demo_auth uniquedemotoken; Type: CONSTRAINT; Schema: public; Owner: palantir
--

ALTER TABLE ONLY sbp_demo_auth
    ADD CONSTRAINT uniquedemotoken UNIQUE (demo_token);


--
-- Name: notification_events uniqueeventid; Type: CONSTRAINT; Schema: public; Owner: palantir
--

ALTER TABLE ONLY notification_events
    ADD CONSTRAINT uniqueeventid UNIQUE (event_id);


--
-- Name: job_leases uniquejobleaseid; Type: CONSTRAINT; Schema: public; Owner: palantir
--

ALTER TABLE ONLY job_leases
    ADD CONSTRAINT uniquejobleaseid UNIQUE (id);


--
-- Name: secrets uniquesecretid; Type: CONSTRAINT; Schema: public; Owner: palantir
--

ALTER TABLE ONLY secrets
    ADD CONSTRAINT uniquesecretid UNIQUE (id);


--
-- Name: stores uniquestoreid; Type: CONSTRAINT; Schema: public; Owner: palantir
--

ALTER TABLE ONLY stores
    ADD CONSTRAINT uniquestoreid UNIQUE (store_id);


--
-- Name: user_cohort_mappings uniqueusercohortmapping; Type: CONSTRAINT; Schema: public; Owner: palantir
--

ALTER TABLE ONLY user_cohort_mappings
    ADD CONSTRAINT uniqueusercohortmapping UNIQUE (cohort_id, user_id);


--
-- Name: app_languages uniqueuserid; Type: CONSTRAINT; Schema: public; Owner: palantir
--

ALTER TABLE ONLY app_languages
    ADD CONSTRAINT uniqueuserid UNIQUE (user_id);


--
-- Name: accounts uniqueuserkey; Type: CONSTRAINT; Schema: public; Owner: palantir
--

ALTER TABLE ONLY accounts
    ADD CONSTRAINT uniqueuserkey UNIQUE (user_id, key);


--
-- Name: user_ids uniqueusernames; Type: CONSTRAINT; Schema: public; Owner: palantir
--

ALTER TABLE ONLY user_ids
    ADD CONSTRAINT uniqueusernames UNIQUE (username);


--
-- Name: notification_prefs uniqueuserprefid; Type: CONSTRAINT; Schema: public; Owner: palantir
--

ALTER TABLE ONLY notification_prefs
    ADD CONSTRAINT uniqueuserprefid UNIQUE (user_id, notification_type);


--
-- Name: user_store_mappings uniqueuserstoremapping; Type: CONSTRAINT; Schema: public; Owner: palantir
--

ALTER TABLE ONLY user_store_mappings
    ADD CONSTRAINT uniqueuserstoremapping UNIQUE (store_id, user_id);


--
-- Name: mobile_devices uniqueuuid; Type: CONSTRAINT; Schema: public; Owner: palantir
--

ALTER TABLE ONLY mobile_devices
    ADD CONSTRAINT uniqueuuid UNIQUE (uuid);


--
-- Name: unsent_emails unsent_emails_pkey; Type: CONSTRAINT; Schema: public; Owner: palantir
--

ALTER TABLE ONLY unsent_emails
    ADD CONSTRAINT unsent_emails_pkey PRIMARY KEY (email_id);


--
-- Name: user_cohorts user_cohorts_pkey; Type: CONSTRAINT; Schema: public; Owner: palantir
--

ALTER TABLE ONLY user_cohorts
    ADD CONSTRAINT user_cohorts_pkey PRIMARY KEY (cohort_id);


--
-- Name: user_ids user_ids_pkey; Type: CONSTRAINT; Schema: public; Owner: palantir
--

ALTER TABLE ONLY user_ids
    ADD CONSTRAINT user_ids_pkey PRIMARY KEY (user_id);


--
-- Name: accountusers; Type: INDEX; Schema: public; Owner: palantir
--

CREATE INDEX accountusers ON accounts USING btree (user_id, key);


--
-- Name: appeventscomponentindex; Type: INDEX; Schema: public; Owner: palantir
--

CREATE INDEX appeventscomponentindex ON app_events USING btree (component);


--
-- Name: appeventstimestampindex; Type: INDEX; Schema: public; Owner: palantir
--

CREATE INDEX appeventstimestampindex ON app_events USING btree (server_ts);


--
-- Name: appeventsuseridindex; Type: INDEX; Schema: public; Owner: palantir
--

CREATE INDEX appeventsuseridindex ON app_events USING btree (user_id);


--
-- Name: authusers; Type: INDEX; Schema: public; Owner: palantir
--

CREATE INDEX authusers ON auth USING btree (user_id);


--
-- Name: confirm_email_addresss_index_by_email_address; Type: INDEX; Schema: public; Owner: palantir
--

CREATE INDEX confirm_email_addresss_index_by_email_address ON confirm_email_address USING btree (email_address);


--
-- Name: confirm_email_addresss_index_by_token; Type: INDEX; Schema: public; Owner: palantir
--

CREATE INDEX confirm_email_addresss_index_by_token ON confirm_email_address USING btree (token);


--
-- Name: email_template_by_name; Type: INDEX; Schema: public; Owner: palantir
--

CREATE INDEX email_template_by_name ON email_templates USING btree (name);


--
-- Name: errornames; Type: INDEX; Schema: public; Owner: palantir
--

CREATE INDEX errornames ON error_reports USING btree (error_name);


--
-- Name: errortimes; Type: INDEX; Schema: public; Owner: palantir
--

CREATE INDEX errortimes ON error_reports USING btree (error_time);


--
-- Name: errorusers; Type: INDEX; Schema: public; Owner: palantir
--

CREATE INDEX errorusers ON error_reports USING btree (user_id);


--
-- Name: erroruuids; Type: INDEX; Schema: public; Owner: palantir
--

CREATE INDEX erroruuids ON error_reports USING btree (error_uuid);


--
-- Name: event; Type: INDEX; Schema: public; Owner: palantir
--

CREATE INDEX event ON email_events USING btree (event);


--
-- Name: goalsglobalstoreid; Type: INDEX; Schema: public; Owner: palantir
--

CREATE INDEX goalsglobalstoreid ON goals_global USING btree (store_id);


--
-- Name: identuserids; Type: INDEX; Schema: public; Owner: palantir
--

CREATE INDEX identuserids ON user_ids USING btree (user_id);


--
-- Name: identusernames; Type: INDEX; Schema: public; Owner: palantir
--

CREATE INDEX identusernames ON user_ids USING btree (username);


--
-- Name: identusertypes; Type: INDEX; Schema: public; Owner: palantir
--

CREATE INDEX identusertypes ON user_ids USING btree (user_type);


--
-- Name: index_by_demo_token; Type: INDEX; Schema: public; Owner: palantir
--

CREATE INDEX index_by_demo_token ON sbp_demo_auth USING btree (demo_token);


--
-- Name: index_by_email_address; Type: INDEX; Schema: public; Owner: palantir
--

CREATE INDEX index_by_email_address ON sbp_demo_auth USING btree (email_address);


--
-- Name: mobile_devices_index_by_ios_token; Type: INDEX; Schema: public; Owner: palantir
--

CREATE INDEX mobile_devices_index_by_ios_token ON mobile_devices USING btree (ios_device_token);


--
-- Name: mobile_devices_index_by_user_id; Type: INDEX; Schema: public; Owner: palantir
--

CREATE INDEX mobile_devices_index_by_user_id ON mobile_devices USING btree (user_id);


--
-- Name: mobile_devices_index_by_uuid; Type: INDEX; Schema: public; Owner: palantir
--

CREATE INDEX mobile_devices_index_by_uuid ON mobile_devices USING btree (uuid);


--
-- Name: objectshashindex; Type: INDEX; Schema: public; Owner: palantir
--

CREATE INDEX objectshashindex ON objects USING btree (hash, type);


--
-- Name: opened_links_index_by_email_id; Type: INDEX; Schema: public; Owner: palantir
--

CREATE INDEX opened_links_index_by_email_id ON sbp_opened_email_links USING btree (email_id);


--
-- Name: opened_links_index_by_redirect_url; Type: INDEX; Schema: public; Owner: palantir
--

CREATE INDEX opened_links_index_by_redirect_url ON sbp_opened_email_links USING btree (redirect_url);


--
-- Name: rolesindexbyuser; Type: INDEX; Schema: public; Owner: palantir
--

CREATE INDEX rolesindexbyuser ON user_roles USING btree (user_id);


--
-- Name: sbp_email_type; Type: INDEX; Schema: public; Owner: palantir
--

CREATE INDEX sbp_email_type ON email_events USING btree (sbp_email_type);


--
-- Name: sbppasswordlogusers; Type: INDEX; Schema: public; Owner: palantir
--

CREATE INDEX sbppasswordlogusers ON sbp_password_log USING btree (user_id);


--
-- Name: secretsbykey; Type: INDEX; Schema: public; Owner: palantir
--

CREATE INDEX secretsbykey ON secrets USING btree (key);


--
-- Name: sent_emails_email_type_idx; Type: INDEX; Schema: public; Owner: palantir
--

CREATE INDEX sent_emails_email_type_idx ON sent_emails USING btree (email_type);


--
-- Name: sent_emails_index_by_email_id; Type: INDEX; Schema: public; Owner: palantir
--

CREATE INDEX sent_emails_index_by_email_id ON sent_emails USING btree (email_id);


--
-- Name: sent_emails_index_by_email_secret; Type: INDEX; Schema: public; Owner: palantir
--

CREATE INDEX sent_emails_index_by_email_secret ON sent_emails USING btree (email_secret);


--
-- Name: sentemailaddressindex; Type: INDEX; Schema: public; Owner: palantir
--

CREATE INDEX sentemailaddressindex ON sent_emails USING btree (email_address);


--
-- Name: socialusers; Type: INDEX; Schema: public; Owner: palantir
--

CREATE INDEX socialusers ON social_media USING btree (user_id, key);


--
-- Name: storebookmarks; Type: INDEX; Schema: public; Owner: palantir
--

CREATE INDEX storebookmarks ON bookmarks USING btree (store_id, type);


--
-- Name: storedfiltersindex; Type: INDEX; Schema: public; Owner: palantir
--

CREATE INDEX storedfiltersindex ON stored_filters USING btree (key);


--
-- Name: storiessearchindex; Type: INDEX; Schema: public; Owner: palantir
--

CREATE INDEX storiessearchindex ON stories USING btree (normalized_target_date, type, store_id);


--
-- Name: storyeventsstoryidindex; Type: INDEX; Schema: public; Owner: palantir
--

CREATE INDEX storyeventsstoryidindex ON story_events USING btree (story_id);


--
-- Name: unsent_emails_index_by_email_id; Type: INDEX; Schema: public; Owner: palantir
--

CREATE INDEX unsent_emails_index_by_email_id ON unsent_emails USING btree (email_id);


--
-- Name: unsent_emails_index_by_email_secret; Type: INDEX; Schema: public; Owner: palantir
--

CREATE INDEX unsent_emails_index_by_email_secret ON unsent_emails USING btree (email_secret);


--
-- Name: unsentemailaddressindex; Type: INDEX; Schema: public; Owner: palantir
--

CREATE INDEX unsentemailaddressindex ON unsent_emails USING btree (email_address);


--
-- Name: usercohortmappingscohortindex; Type: INDEX; Schema: public; Owner: palantir
--

CREATE INDEX usercohortmappingscohortindex ON user_cohort_mappings USING btree (cohort_id);


--
-- Name: usercohortmappingsuserindex; Type: INDEX; Schema: public; Owner: palantir
--

CREATE INDEX usercohortmappingsuserindex ON user_cohort_mappings USING btree (user_id);


--
-- Name: userids; Type: INDEX; Schema: public; Owner: palantir
--

CREATE INDEX userids ON app_languages USING btree (user_id);


--
-- Name: userstorebookmarkkey; Type: INDEX; Schema: public; Owner: palantir
--

CREATE INDEX userstorebookmarkkey ON bookmarks USING btree (user_id, store_id, type, key, deleted);


--
-- Name: userstorebookmarks; Type: INDEX; Schema: public; Owner: palantir
--

CREATE INDEX userstorebookmarks ON bookmarks USING btree (user_id, store_id, type);


--
-- Name: userstoremappingsstoreindex; Type: INDEX; Schema: public; Owner: palantir
--

CREATE INDEX userstoremappingsstoreindex ON user_store_mappings USING btree (store_id);


--
-- Name: userstoremappingsuserindex; Type: INDEX; Schema: public; Owner: palantir
--

CREATE INDEX userstoremappingsuserindex ON user_store_mappings USING btree (user_id);


--
-- Name: public; Type: ACL; Schema: -; Owner: palantir
--

GRANT USAGE ON SCHEMA public TO read_only;


--
-- Name: account_confirmation; Type: ACL; Schema: public; Owner: palantir
--

GRANT SELECT ON TABLE account_confirmation TO read_only;


--
-- Name: accounts; Type: ACL; Schema: public; Owner: palantir
--

GRANT SELECT ON TABLE accounts TO read_only;
GRANT SELECT ON TABLE accounts TO sbp_user;


--
-- Name: alerts; Type: ACL; Schema: public; Owner: palantir
--

GRANT SELECT ON TABLE alerts TO read_only;


--
-- Name: alerts_alert_id_seq; Type: ACL; Schema: public; Owner: palantir
--

GRANT SELECT ON SEQUENCE alerts_alert_id_seq TO read_only;


--
-- Name: app_alerts; Type: ACL; Schema: public; Owner: palantir
--

GRANT SELECT ON TABLE app_alerts TO read_only;


--
-- Name: app_alerts_app_alert_id_seq; Type: ACL; Schema: public; Owner: palantir
--

GRANT SELECT ON SEQUENCE app_alerts_app_alert_id_seq TO read_only;


--
-- Name: app_events; Type: ACL; Schema: public; Owner: palantir
--

GRANT SELECT ON TABLE app_events TO read_only;


--
-- Name: app_languages; Type: ACL; Schema: public; Owner: palantir
--

GRANT SELECT ON TABLE app_languages TO read_only;


--
-- Name: auth; Type: ACL; Schema: public; Owner: palantir
--

GRANT SELECT ON TABLE auth TO read_only;


--
-- Name: bookmarks; Type: ACL; Schema: public; Owner: palantir
--

GRANT SELECT ON TABLE bookmarks TO read_only;


--
-- Name: bookmarks_id_seq; Type: ACL; Schema: public; Owner: palantir
--

GRANT SELECT ON SEQUENCE bookmarks_id_seq TO read_only;


--
-- Name: clover_app_install_statuses; Type: ACL; Schema: public; Owner: palantir
--

GRANT SELECT ON TABLE clover_app_install_statuses TO read_only;


--
-- Name: clover_data_pipeline_status; Type: ACL; Schema: public; Owner: palantir
--

GRANT SELECT ON TABLE clover_data_pipeline_status TO read_only;


--
-- Name: clover_features; Type: ACL; Schema: public; Owner: palantir
--

GRANT SELECT ON TABLE clover_features TO read_only;


--
-- Name: clover_smart_auth; Type: ACL; Schema: public; Owner: palantir
--

GRANT SELECT ON TABLE clover_smart_auth TO read_only;


--
-- Name: clover_smart_auth_credentials; Type: ACL; Schema: public; Owner: palantir
--

GRANT SELECT ON TABLE clover_smart_auth_credentials TO read_only;


--
-- Name: clover_smart_auth_errors; Type: ACL; Schema: public; Owner: palantir
--

GRANT SELECT ON TABLE clover_smart_auth_errors TO read_only;


--
-- Name: clover_smart_auth_errors_clover_smart_auth_error_id_seq; Type: ACL; Schema: public; Owner: palantir
--

GRANT SELECT ON SEQUENCE clover_smart_auth_errors_clover_smart_auth_error_id_seq TO read_only;


--
-- Name: clover_smart_auth_log_id_seq; Type: ACL; Schema: public; Owner: palantir
--

GRANT SELECT ON SEQUENCE clover_smart_auth_log_id_seq TO read_only;


--
-- Name: cohorts; Type: ACL; Schema: public; Owner: palantir
--

GRANT SELECT ON TABLE cohorts TO read_only;


--
-- Name: confirm_email_address; Type: ACL; Schema: public; Owner: palantir
--

GRANT SELECT ON TABLE confirm_email_address TO read_only;


--
-- Name: email_events; Type: ACL; Schema: public; Owner: palantir
--

GRANT SELECT ON TABLE email_events TO read_only;


--
-- Name: email_registration; Type: ACL; Schema: public; Owner: palantir
--

GRANT SELECT ON TABLE email_registration TO read_only;


--
-- Name: email_templates; Type: ACL; Schema: public; Owner: palantir
--

GRANT SELECT ON TABLE email_templates TO read_only;


--
-- Name: empathy; Type: ACL; Schema: public; Owner: palantir
--

GRANT SELECT ON TABLE empathy TO read_only;


--
-- Name: error_reports; Type: ACL; Schema: public; Owner: palantir
--

GRANT SELECT ON TABLE error_reports TO read_only;


--
-- Name: feedbacks; Type: ACL; Schema: public; Owner: palantir
--

GRANT SELECT ON TABLE feedbacks TO read_only;


--
-- Name: stores; Type: ACL; Schema: public; Owner: palantir
--

GRANT SELECT ON TABLE stores TO read_only;


--
-- Name: user_ids; Type: ACL; Schema: public; Owner: palantir
--

GRANT SELECT ON TABLE user_ids TO read_only;


--
-- Name: user_store_mappings; Type: ACL; Schema: public; Owner: palantir
--

GRANT SELECT ON TABLE user_store_mappings TO read_only;


--
-- Name: full_user_store_mappings_vw; Type: ACL; Schema: public; Owner: palantir
--

GRANT SELECT ON TABLE full_user_store_mappings_vw TO read_only;


--
-- Name: goals_global; Type: ACL; Schema: public; Owner: palantir
--

GRANT SELECT ON TABLE goals_global TO read_only;


--
-- Name: job_leases; Type: ACL; Schema: public; Owner: palantir
--

GRANT SELECT ON TABLE job_leases TO read_only;


--
-- Name: key_values; Type: ACL; Schema: public; Owner: palantir
--

GRANT SELECT ON TABLE key_values TO read_only;


--
-- Name: locations; Type: ACL; Schema: public; Owner: palantir
--

GRANT SELECT ON TABLE locations TO read_only;


--
-- Name: mobile_devices; Type: ACL; Schema: public; Owner: palantir
--

GRANT SELECT ON TABLE mobile_devices TO read_only;


--
-- Name: mobile_devices_id_seq; Type: ACL; Schema: public; Owner: palantir
--

GRANT SELECT ON SEQUENCE mobile_devices_id_seq TO read_only;


--
-- Name: notification_events; Type: ACL; Schema: public; Owner: palantir
--

GRANT SELECT ON TABLE notification_events TO read_only;


--
-- Name: notification_prefs; Type: ACL; Schema: public; Owner: palantir
--

GRANT SELECT ON TABLE notification_prefs TO read_only;


--
-- Name: notifications; Type: ACL; Schema: public; Owner: palantir
--

GRANT SELECT ON TABLE notifications TO read_only;


--
-- Name: notifications_notification_id_seq; Type: ACL; Schema: public; Owner: palantir
--

GRANT SELECT ON SEQUENCE notifications_notification_id_seq TO read_only;


--
-- Name: notifications_v2; Type: ACL; Schema: public; Owner: palantir
--

GRANT SELECT ON TABLE notifications_v2 TO read_only;


--
-- Name: notifications_v2_notification_id_seq; Type: ACL; Schema: public; Owner: palantir
--

GRANT SELECT ON SEQUENCE notifications_v2_notification_id_seq TO read_only;


--
-- Name: notifications_v3; Type: ACL; Schema: public; Owner: palantir
--

GRANT SELECT ON TABLE notifications_v3 TO read_only;


--
-- Name: objects; Type: ACL; Schema: public; Owner: palantir
--

GRANT SELECT ON TABLE objects TO read_only;


--
-- Name: sbp_auth; Type: ACL; Schema: public; Owner: palantir
--

GRANT SELECT ON TABLE sbp_auth TO read_only;


--
-- Name: sbp_demo_auth; Type: ACL; Schema: public; Owner: palantir
--

GRANT SELECT ON TABLE sbp_demo_auth TO read_only;


--
-- Name: sbp_opened_email_links; Type: ACL; Schema: public; Owner: palantir
--

GRANT SELECT ON TABLE sbp_opened_email_links TO read_only;


--
-- Name: sbp_password_log; Type: ACL; Schema: public; Owner: palantir
--

GRANT SELECT ON TABLE sbp_password_log TO read_only;


--
-- Name: sbp_password_reset; Type: ACL; Schema: public; Owner: palantir
--

GRANT SELECT ON TABLE sbp_password_reset TO read_only;


--
-- Name: secrets; Type: ACL; Schema: public; Owner: palantir
--

GRANT SELECT ON TABLE secrets TO read_only;
GRANT INSERT ON TABLE secrets TO analytics_prod;


--
-- Name: sent_emails; Type: ACL; Schema: public; Owner: palantir
--

GRANT SELECT ON TABLE sent_emails TO read_only;
GRANT INSERT,UPDATE ON TABLE sent_emails TO ready_only_except_email;


--
-- Name: sent_emails_email_id_seq; Type: ACL; Schema: public; Owner: palantir
--

GRANT SELECT ON SEQUENCE sent_emails_email_id_seq TO read_only;
GRANT UPDATE ON SEQUENCE sent_emails_email_id_seq TO ready_only_except_email;


--
-- Name: sent_emails_prereg; Type: ACL; Schema: public; Owner: palantir
--

GRANT SELECT ON TABLE sent_emails_prereg TO read_only;


--
-- Name: social_media; Type: ACL; Schema: public; Owner: palantir
--

GRANT SELECT ON TABLE social_media TO read_only;


--
-- Name: stored_filters; Type: ACL; Schema: public; Owner: palantir
--

GRANT SELECT ON TABLE stored_filters TO read_only;


--
-- Name: stored_filters_temp; Type: ACL; Schema: public; Owner: palantir
--

GRANT SELECT ON TABLE stored_filters_temp TO read_only;


--
-- Name: stores_store_id_seq; Type: ACL; Schema: public; Owner: palantir
--

GRANT SELECT ON SEQUENCE stores_store_id_seq TO read_only;


--
-- Name: stores_temp; Type: ACL; Schema: public; Owner: palantir
--

GRANT SELECT ON TABLE stores_temp TO read_only;


--
-- Name: stories; Type: ACL; Schema: public; Owner: palantir
--

GRANT SELECT ON TABLE stories TO read_only;


--
-- Name: stories_dev; Type: ACL; Schema: public; Owner: palantir
--

GRANT SELECT ON TABLE stories_dev TO read_only;


--
-- Name: stories_id_seq; Type: ACL; Schema: public; Owner: palantir
--

GRANT SELECT ON SEQUENCE stories_id_seq TO read_only;


--
-- Name: story_events; Type: ACL; Schema: public; Owner: palantir
--

GRANT SELECT ON TABLE story_events TO read_only;


--
-- Name: system_properties; Type: ACL; Schema: public; Owner: palantir
--

GRANT SELECT ON TABLE system_properties TO read_only;


--
-- Name: unsent_emails; Type: ACL; Schema: public; Owner: palantir
--

GRANT SELECT ON TABLE unsent_emails TO read_only;


--
-- Name: unsent_emails_email_id_seq; Type: ACL; Schema: public; Owner: palantir
--

GRANT SELECT ON SEQUENCE unsent_emails_email_id_seq TO read_only;


--
-- Name: unsent_emails_prereg; Type: ACL; Schema: public; Owner: palantir
--

GRANT SELECT ON TABLE unsent_emails_prereg TO read_only;


--
-- Name: user_cohort_mappings; Type: ACL; Schema: public; Owner: palantir
--

GRANT SELECT ON TABLE user_cohort_mappings TO read_only;


--
-- Name: user_cohorts; Type: ACL; Schema: public; Owner: palantir
--

GRANT SELECT ON TABLE user_cohorts TO read_only;


--
-- Name: user_cohorts_cohort_id_seq; Type: ACL; Schema: public; Owner: palantir
--

GRANT SELECT ON SEQUENCE user_cohorts_cohort_id_seq TO read_only;


--
-- Name: user_ids_user_id_seq; Type: ACL; Schema: public; Owner: palantir
--

GRANT SELECT ON SEQUENCE user_ids_user_id_seq TO read_only;


--
-- Name: user_indexed_cohorts; Type: ACL; Schema: public; Owner: palantir
--

GRANT SELECT ON TABLE user_indexed_cohorts TO read_only;


--
-- Name: user_roles; Type: ACL; Schema: public; Owner: palantir
--

GRANT SELECT ON TABLE user_roles TO read_only;


--
-- Name: x; Type: ACL; Schema: public; Owner: palantir
--

GRANT SELECT ON TABLE x TO read_only;


--
-- Name: DEFAULT PRIVILEGES FOR SEQUENCES; Type: DEFAULT ACL; Schema: public; Owner: palantir
--

ALTER DEFAULT PRIVILEGES FOR ROLE palantir IN SCHEMA public REVOKE ALL ON SEQUENCES  FROM palantir;
ALTER DEFAULT PRIVILEGES FOR ROLE palantir IN SCHEMA public GRANT SELECT ON SEQUENCES  TO read_only;


--
-- Name: DEFAULT PRIVILEGES FOR FUNCTIONS; Type: DEFAULT ACL; Schema: public; Owner: palantir
--

ALTER DEFAULT PRIVILEGES FOR ROLE palantir IN SCHEMA public REVOKE ALL ON FUNCTIONS  FROM PUBLIC;
ALTER DEFAULT PRIVILEGES FOR ROLE palantir IN SCHEMA public REVOKE ALL ON FUNCTIONS  FROM palantir;
ALTER DEFAULT PRIVILEGES FOR ROLE palantir IN SCHEMA public GRANT ALL ON FUNCTIONS  TO read_only;


--
-- Name: DEFAULT PRIVILEGES FOR TABLES; Type: DEFAULT ACL; Schema: public; Owner: palantir
--

ALTER DEFAULT PRIVILEGES FOR ROLE palantir IN SCHEMA public REVOKE ALL ON TABLES  FROM palantir;
ALTER DEFAULT PRIVILEGES FOR ROLE palantir IN SCHEMA public GRANT SELECT ON TABLES  TO read_only;


--
-- PostgreSQL database dump complete
--
