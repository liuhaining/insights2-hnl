--Load resolutions created from pior haduken job into the database mapping table

--ugly way to throw an exception
SELECT 1/COUNT(*) FROM $FACTUAL_SCHEMA.staging_resolutions_unique;

TRUNCATE TABLE $FACTUAL_SCHEMA.factual_resolutions;

INSERT into $FACTUAL_SCHEMA.factual_resolutions 
	(merchant_id, 
	platform_id, 
	factual_id, 
	confidence) 
select 
	cast(merchant_id AS INTEGER), 
	cast(platform_id AS INTEGER), 
	factual_id, 
	confidence
from $FACTUAL_SCHEMA.staging_resolutions_unique;

COMMIT;

select analyze_histogram('$FACTUAL_SCHEMA.factual_resolutions', 100);

DROP TABLE $FACTUAL_SCHEMA.staging_resolutions_unique;