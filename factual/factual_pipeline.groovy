// factual_pipeline.groovy
// A wrapper script for running the Factual pipeline after DIS lands jsons.
// Run this script until all Factual files are ingested, and then run 
// factual_recreate_snapshot to generate the Factual snapshot table and Haduken matches.


import com.google.common.base.Preconditions
import groovy.json.*

/***************************************************************************************************
 * PARSE COMMAND LINE OPTIONS
 **************************************************************************************************/

// DIS dataset
Preconditions.checkArgument(!!args.dataset, 'no DIS dataset specified')
def DIS_DATASET = args.dataset

// DIS dataset transaction number required
Preconditions.checkArgument(!!args.txn, 'no DIS transaction specified')
def DIS_TXN_NO = args.txn

// the environment to run the pipeline in, either 'qa' or 'prod' (default: qa)
def ENV = System.getenv("ENV")
def CWD = System.getenv("CWD")
def CLUSTER = System.getenv("CLUSTER");

def parseMetadata(disDatasetsDir, disDataset, disTxnNo) {
    def metadata_path = "$disDatasetsDir/$disDataset/$disTxnNo/main/metadata.json"
    def metadata_json = "hadoop fs -cat $metadata_path".execute().text
    def metadata = new JsonSlurper().parseText(metadata_json)
    Preconditions.checkArgument(!!metadata, "[$metadata_path] does not contain parsable json")

    return [
        metadata_path:metadata_path,
        source_yyyymm:metadata['source_date'].substring(0,6),
        source_date:metadata['source_date'],
        source_cat:metadata['source_cat'],
        source_file:metadata['source_file'],
    ]
}



/***************************************************************************************************
 * PARAMETER DEFINITIONS
 **************************************************************************************************/

def now                   = new Date()
def now_formatted         = now.format("yyyyMMdd")
def NOW_TS                = now.time.toString()

// Initialize env params

def HIVE_SCHEMA = CLUSTER == "legacy" ? ENV : "${CLUSTER}_${ENV}"
def DB_RAW_SCHEMA = "raw_${ENV}"
def DB_PROD_SCHEMA = "fletcher_${ENV}"
def FACTUAL_SCHEMA = "factual_${ENV}"
def PIPELINES_CODE_DIR = "${CWD}"
def VSQL_CMD_WARGS = System.getenv("DB_CONNECT")
def S3E_LOGS_DIR = "s3e://pcloud-valhalla-pipeline-maroon/log/factual-${ENV}"
def S3E_DATA_DIR = 's3e://pcloud-valhalla-pipeline-maroon/data'

// parse the source_date and type of Factual file.
def info = parseMetadata(S3E_DATA_DIR, DIS_DATASET, DIS_TXN_NO)
def SOURCE_FILE                 = info.source_file
def SOURCE_CAT                  = info.source_cat
def SOURCE_DATE                 = info.source_date
def SOURCE_MONTH                = info.source_yyyymm
def METADATA_JSON_PATH          = info.metadata_path
def DIS_FILES_GLOB              = METADATA_JSON_PATH.replace('metadata.json','[!metadata.json]*')
def IMPORT_TS                   = new Date().format("yyyy-MM-dd HH:mm:ss")



// Dependent params

def SOURCE_DELETE_FILE    = "delete-$SOURCE_FILE"

def JSON_FP               = "/data/landing/factual/$SOURCE_FILE"
  
def SOURCE_DATE_LOGS      = now.format("yyyyMMdd")

def LANDING_JSON_LOAD_COND      = "source_date = '$SOURCE_DATE' AND source_cat = '$SOURCE_CAT'" + 
                                  " AND source_file = '$SOURCE_FILE' "
  
def SCRATCH_DIR_FULL      = "/data/landing/scratch_space/$CLUSTER/$SOURCE_FILE"
def SCRATCH_DIR           = "$SOURCE_FILE"
def TMP_SQL_FILE          = new File(SCRATCH_DIR_FULL, "factual.tmp.sql")
def HIVE_CMD              = 'hive'
def LANDING_JSON_TBL      = 'landing_json_factual'
def LOCAL_JOINED_FILE     = "$SOURCE_FILE"
def SOURCE_FILE_GZ        = "$LOCAL_JOINED_FILE" + ".gz"
def SCRATCH_DIR_FULL_GZ   = "$SCRATCH_DIR_FULL/$LOCAL_JOINED_FILE" + ".gz"
def DB_STAGING_TBL        = 'db_staging_factual'
def FACTUAL_VERSIONED_TBL = 'factual_versioned'

name "Factual"  
audit SOURCE_FILE, "vertica"



def S3E_HIVE_SCHEMAS_DIR        = 's3e://pcloud-valhalla-pipeline-maroon/hive'
def HDFS_HIVE_SCHEMAS_DIR       = '/hive'

def HDFS_DB_STAGING_DIR         = "$HDFS_HIVE_SCHEMAS_DIR/$HIVE_SCHEMA/$DB_STAGING_TBL"
def DB_STAGING_FP               = "$SCRATCH_DIR_FULL/factual.dlm"
def HDFS_LOGS_ARCHIVE_DIR 	    = "$S3E_LOGS_DIR/$SOURCE_MONTH/$SOURCE_DATE_LOGS"
def HADUKEN_NAME 		            = "staging"
def LANDING_JSON_PART_FP        = "$S3E_HIVE_SCHEMAS_DIR/$HIVE_SCHEMA/$LANDING_JSON_TBL/" + 
                                  "source_date=$SOURCE_DATE/source_cat=$SOURCE_CAT/" + 
                                  "source_file=$SOURCE_FILE"

def executeTemplate = { src, cmdWArgs, binding -> return {
        def text = new File(src).text
        for (def entry : binding) {
        println "Processing entry ${entry.key} and ${entry.value}"	
	    text = text.replaceAll("\\\$${entry.key}", entry.value)
        }
        TMP_SQL_FILE.text = text
        exec cmdWArgs
    }
}

def hiveTableRows = { table ->
  return """  $HIVE_CMD -S -e "SELECT COUNT(*) FROM $table
    WHERE
    source_date = '$SOURCE_DATE' AND
    source_cat = '$SOURCE_CAT' AND
    source_file = '$SOURCE_FILE';" """
} 


println "Factual - processing filename ${SOURCE_FILE}"

// make the temporary directory for all file operations.
step "mk_scratch_dir", {
    completed "test -d $SCRATCH_DIR_FULL"
    command   "mkdir -p $SCRATCH_DIR_FULL"
    metric    "true"
    rollback  "true"
}

// query hadoop for the file parts, join them back together and place the result in the scratch directory.
// note: when 'cat'ting files from hadoop, the resulting file is zipped.
step "rejoin_split_files_from_s3e", {
   completed "test -e ${SCRATCH_DIR_FULL_GZ}"
   command "hadoop fs -cat ${S3E_DATA_DIR}/${DIS_DATASET}/${DIS_TXN_NO}/main/f* > ${SCRATCH_DIR_FULL_GZ}"
   metric  "true"
   rollback "true"
}

// place the raw JSON file into the HIVE Factual landing table.
// the HIVE Factual landing table will store every row into a single unprocessed column. 
step "load_joined_json_to_hive", {
      completed "false"
      command   executeTemplate("$PIPELINES_CODE_DIR/hive/hive_load_json.sql", 
                  "$HIVE_CMD -f $TMP_SQL_FILE", [HIVE_SCHEMA:HIVE_SCHEMA,LANDING_JSON_TBL:LANDING_JSON_TBL,SOURCE_DATE:SOURCE_DATE,SOURCE_CAT:SOURCE_CAT,SOURCE_FILE:SOURCE_FILE,SCRATCH_DIR_FULL_GZ:SCRATCH_DIR_FULL_GZ,LANDING_JSON_PART_FP:LANDING_JSON_PART_FP])
     // metric    "rows", hiveTableMetric('COUNT(*)', LANDING_JSON_TBL)
      metric    "true"
      rollback  "true"
}


// load the hive data from the landing table into the hive factual staging table.
// The HIVE Factual Staging table takes the single unprocessed row of data and separates it
// into individual fields so that it can be transferred to Vertica successfully in the next step.
step "db_staging_load_from_json", {
   completed "false"
   command   executeTemplate("$PIPELINES_CODE_DIR/factual_db_staging_load.sql", 
                "$HIVE_CMD -f $TMP_SQL_FILE", [HIVE_SCHEMA:HIVE_SCHEMA,DB_STAGING_TBL:DB_STAGING_TBL,SOURCE_DATE:SOURCE_DATE,SOURCE_CAT:SOURCE_CAT,SOURCE_FILE:SOURCE_FILE,NOW_TS:NOW_TS,LANDING_JSON_TBL:LANDING_JSON_TBL])
   metric    "rows", "$HIVE_CMD -S -e 'SELECT COUNT(*) FROM ${HIVE_SCHEMA}.${DB_STAGING_TBL};'"
   rollback  "true"
}


// load data from HIVE factual staging table into the Vertica Factual versioned table.
step "versioned_tbl_load_factual", {
    completed "false" 
    command     """hadoop fs -cat $HDFS_DB_STAGING_DIR/source_date=${SOURCE_DATE}/source_file=${SOURCE_FILE}/* | dd bs=1M | $VSQL_CMD_WARGS -c "
                    COPY ${DB_RAW_SCHEMA}.${FACTUAL_VERSIONED_TBL}
                    FROM LOCAL STDIN
                    DELIMITER E'\001'
                    NULL AS '\\N'
                    EXCEPTIONS '${SCRATCH_DIR_FULL}/copy_data.excepts'
                    REJECTED DATA '${SCRATCH_DIR_FULL}/copy_data.rejects'
                " """
    metric "true"
    rollback  "true"
}

// -- cleanup steps

// clean up all incidental temporary files from the Hive tables.
step "rm_local_db_staging_files", {
    completed "! test -e $DB_STAGING_FP"
    command   "rm $DB_STAGING_FP $SCRATCH_DIR_FULL/.*crc"
    metric    "true"
    rollback  "true"
}

// tar.gz the scratch directory and delete the original.
step "tar_gz_scratch_dir", {
    completed "false"
    command   """   cd $SCRATCH_DIR_FULL/.. && 
                    tar -cvzf ${SCRATCH_DIR}.tar.gz $SCRATCH_DIR && 
                    rm -r $SCRATCH_DIR """
    metric    "true"
    rollback  "true"
}

// create an archive directory in HDFS for all logs.
step "mkdir_logs_archive_date", {
    completed "hadoop fs -test -e $HDFS_LOGS_ARCHIVE_DIR"
    command   "hadoop fs -mkdir $HDFS_LOGS_ARCHIVE_DIR"
    metric    "true"
    rollback  "true"
}

// remove any existing archived logs in HDFS for this file.
step "rm_archived_logs", {
   completed  "! hadoop fs -test -e $HDFS_LOGS_ARCHIVE_DIR/${SCRATCH_DIR}.tar.gz"
   command    "hadoop fs -rm $HDFS_LOGS_ARCHIVE_DIR/${SCRATCH_DIR}.tar.gz"
   metric     "true"
   rollback   "true"
}

// copy the archived logs into HDFS.
step "archive_logs", {
   completed  "false"
   command    """   hadoop fs -put ${SCRATCH_DIR_FULL}.tar.gz $HDFS_LOGS_ARCHIVE_DIR &&
                    rm ${SCRATCH_DIR_FULL}.tar.gz  """
   metric     "true"
   rollback   "true"
}

