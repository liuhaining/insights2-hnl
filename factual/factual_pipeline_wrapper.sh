#!/bin/bash
# A wrapper script for running the Factual pipeline after DIS lands jsons/
# Run this script until all Factual files are ingested, and then run 
# factual_recreate_snapshot to generate Haduken matches.
#
# Usage: sh factual_wrapper.sh DIS_DATASET DIS_TXN_NO ENV [SCRIPT_ARGS]
#   DIS_DATASET     : the DIS dataset name (e.g., north-settle)
#   DIS_TXN_NO      : the DIS dataset transaction number (e.g, 23)
#   ENV             : (optional) [prod|qa] whether to use production or QA environments (default: prod)

# Determine current working directory based on this script's location
export CWD=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
# Common directory
COMMON_DIR=${CWD}/../../common

# This script's name
NAME="factual"
DESCRIPTION="A wrapper script for running the Factual pipeline"
# Groovy Script this wrapper executes
MATILDA_SCRIPT="${CWD}/factual_pipeline.groovy"


# These functions must be declared prior to call to init.sh
# This function just processes/parses extra arguments passed with -o
function process_local_arguments {
    printf "Executing local process_local_arguments with arguments $OPTIONAL_ARGS\n"
    # No additional parsing required here
    ARGS=${OPTIONAL_ARGS}
    # Removing stupid rundeck quotes, if any
    temp="${ARGS%\"}"
    ARGS="${temp#\"}"

    IFS=' ' read -a ARGUMENT_ARRAY <<< "${ARGS}"

    for keyvalue in "${ARGUMENT_ARRAY[@]}"
    do
        KEY_VALUE_ARRAY=(${keyvalue//=/ })
        

        case "${KEY_VALUE_ARRAY[0]}" in

            'DIS_DATASET')  
                DIS_DATASET="${KEY_VALUE_ARRAY[1]}"
                echo "DIS_DATASET is ${DIS_DATASET}"    
                ;;
            'DIS_TXN_NO') 
                DIS_TXN_NO="${KEY_VALUE_ARRAY[1]}"
                echo  "DIS_TXN_NO is ${DIS_TXN_NO}"
                ;;
            *)  printf "\nArguments for this shell  script must be named DIS_DATASET and DIS_TXN_NO\n"
                printf "Ex: factual_pipeline_wrapper.sh -e prod -o 'DIS_DATASET=somename DIS_TXN_NO=12'"
                __die
                ;;
        esac 
    done
}

# This function shows additional help content
function show_more_help {
    printf ' \t -o other arguments to be passed to the matilda script (e.g., -o "-D key=value key=value") \n'
    printf ' \t IMPORTANT NOTE: if you are to use -o "arg_values" and your arg_values contain spaces you need to surround them with quotes like shown above\n'
}

# Process arguments, source environment
source $COMMON_DIR/init.sh

# Log file for this process - these statements must occur after sourcing init.sh
# env-$ENV.sh declares $LOCAL_LOG_LOC
DATE=$(date "+%Y%m%d")
LOGNAME="${NAME}.${DATE}.log"
LOCAL_LOG="${LOCAL_LOG_LOC}/${NAME}/${LOGNAME}"
HDFS_LOG="${HDFS_LOG_LOC}/${NAME}/${DIS_DATASET}.${DATE}.log"
export HDFS_DATA="s3e://pcloud-valhalla-pipeline-maroon/data/${NAME}/${DIS_TXN_NO}/main/"
__announcement

if [ ! -d "${LOCAL_LOG_LOC}/${NAME}/" ]; then 
    mkdir -p ${LOCAL_LOG_LOC}/${NAME}/
fi             

# Check if JAVA_HOME is defined (in init.sh)
__check_java

# Fire it up. $MATILDA_LIB comes in from env-$ENV.sh being sourced (initialized by) init.sh

# # execute pipeline, appending splitting output to log
cd /data/landing/scratch_space # so that any random Hive files get written here
${JAVA_HOME}/bin/java ${MATILDA_JAVA_OPTS} -jar ${MATILDA_LIB} ${MATILDA_SCRIPT_OPTS} -D dataset=${DIS_DATASET} -D txn=${DIS_TXN_NO} -D filename=${FILENAME} --script ${MATILDA_SCRIPT} |& tee -a $LOCAL_LOG
