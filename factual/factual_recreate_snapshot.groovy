import com.google.common.base.Preconditions

/***************************************************************************************************
 * PARSE COMMAND LINE OPTIONS
 **************************************************************************************************/

// the environment to run the pipeline in, either 'qa' or 'prod' (default: qa)
def ENV = System.getenv("ENV")

println "Running Haduken merchant matching job and recreating Factual snapshot table"

def CWD = System.getenv("CWD")
def CLUSTER = System.getenv("CLUSTER")

/***************************************************************************************************
 * PARAMETER DEFINITIONS
 **************************************************************************************************/

def now                   = new Date()
def now_formatted         = now.format("yyyyMMdd")
def now_ts                = now.time

// Initialize env params

def HIVE_SCHEMA = CLUSTER == "legacy" ? ENV : "${CLUSTER}_${ENV}"
def TXNS_TBL = CLUSTER == "legacy" ? "transactions_v6" : "transactions_snapshot_vw_v3"
def DB_RAW_SCHEMA = "raw_${ENV}"
def DB_PROD_SCHEMA = "fletcher_${ENV}"
def FACTUAL_SCHEMA = "factual_${ENV}"
def PIPELINES_CODE_DIR = "${CWD}"
def VSQL_CMD_WARGS = System.getenv("DB_CONNECT")
def S3E_LOGS_DIR = "s3e://pcloud-valhalla-pipeline-maroon/log/factual-${ENV}"
def S3E_DATA_DIR = 's3e://pcloud-valhalla-pipeline-maroon/data/factual/'

// Dependent params 
def SOURCE_DATE           = "$now_formatted"
def SOURCE_MONTH          = now.format("yyyyMM")
def SOURCE_DATE_LOGS      = now.format("yyyyMMdd")
def SOURCE_CAT            = "factual"


def SOURCE_FILE           = "factual-recreate-snapshot_$SOURCE_DATE"  
def SCRATCH_DIR_FULL      = "/data/landing/scratch_space/$CLUSTER/$SOURCE_FILE"
def SCRATCH_DIR           = "$SOURCE_FILE"
def TMP_SQL_FILE          = new File(SCRATCH_DIR_FULL, "factual.tmp.sql")
def HIVE_CMD              = 'hive'
def LANDING_JSON_TBL      = 'landing_json_factual'
def LOCAL_JOINED_FILE     = "$SOURCE_FILE"
def SCRATCH_DIR_FULL_GZ   = "$SCRATCH_DIR_FULL/$LOCAL_JOINED_FILE" + ".gz"
def DB_STAGING_TBL        = 'db_staging_factual'
def FACTUAL_VERSIONED_TBL = 'factual_versioned'

def HAD_LANDING_DIR	  	  = "/data/landing/factual/$CLUSTER"  //haduken landing dir
//haduken dataset dir;hardcoding in haduken limit this to 1 environment at a time
def HAD_DATASET_DIR		  = "/user/palantir/datasets"  

name "Factual-Recreate-Snapshot"  
audit SOURCE_FILE, "vertica"



def S3E_HIVE_SCHEMAS_DIR        = 's3e://pcloud-valhalla-pipeline-maroon/hive'
def HDFS_HIVE_SCHEMAS_DIR       = '/hive'

def HDFS_DB_STAGING_DIR         = "$HDFS_HIVE_SCHEMAS_DIR/$HIVE_SCHEMA/$DB_STAGING_TBL"
def DB_STAGING_FP               = "$SCRATCH_DIR_FULL/factual.dlm"
def HDFS_LOGS_ARCHIVE_DIR 	= "$S3E_LOGS_DIR/$SOURCE_MONTH/$SOURCE_DATE_LOGS"
def HADUKEN_NAME 		= "staging"
def LANDING_JSON_PART_FP        = "$S3E_HIVE_SCHEMAS_DIR/$HIVE_SCHEMA/$LANDING_JSON_TBL/" + 
                                  "source_date=$SOURCE_DATE/source_cat=$SOURCE_CAT/" + 
                                  "source_file=$SOURCE_FILE"


def executeTemplate = { src, cmdWArgs, binding -> return {
        def text = new File(src).text
        for (def entry : binding) {
        println "Processing entry ${entry.key} and ${entry.value}"	
	    text = text.replaceAll("\\\$${entry.key}", entry.value)
        }
        TMP_SQL_FILE.text = text
        exec cmdWArgs
    }
}

def hiveTableRows = { table ->
  return """  $HIVE_CMD -S -e "SELECT COUNT(*) FROM $table
    WHERE
    source_date = '$SOURCE_DATE' AND
    source_cat = '$SOURCE_CAT' AND
    source_file = '$SOURCE_FILE';" """
} 

// make the temporary directory for all file operations.
step "mk_scratch_dir", {
    completed "test -d $SCRATCH_DIR_FULL"
    command   "mkdir -p $SCRATCH_DIR_FULL"
    metric    "true"
    rollback  "true"
}

// compute the Factual snapshot table from existing data in the factual versioned table.
step "recreate_snapshot", {
     completed "false"
     command   executeTemplate("$PIPELINES_CODE_DIR/factual_recreate_snapshot.sql",
                 "$VSQL_CMD_WARGS -f $TMP_SQL_FILE", 
				 [FACTUAL_SCHEMA:FACTUAL_SCHEMA,
					 DB_RAW_SCHEMA:DB_RAW_SCHEMA,
					 FACTUAL_VERSIONED_TBL:FACTUAL_VERSIONED_TBL])
     metric    "true"
     rollback  "true"
}

// remove Haduken files before starting, in case they exist.

step "remove_haduken_files", {
   completed "false"
   command   "hadoop dfs -rm -f $HAD_DATASET_DIR/$HADUKEN_NAME-fletcher-merchants && hadoop dfs -rm -f $HAD_DATASET_DIR/$HADUKEN_NAME-global-places && rm -f $HAD_LANDING_DIR/$HADUKEN_NAME-fletcher-merchants && rm -f $HAD_LANDING_DIR/$HADUKEN_NAME-global-places"
   metric    "true"
   rollback  "true"
}



// run haduken steps to create matches.
// make the temporary directory for all file operations.
step "mk_landing_dir", {
	completed "test -d $HAD_LANDING_DIR"
	command   "mkdir -p $HAD_LANDING_DIR"
	metric    "true"
	rollback  "true"
}


// make the directory for haduken file operations.
step "mk_dataset_dir", {
	completed "test -d $HAD_LANDING_DIR"
	command   "mkdir -p $HAD_LANDING_DIR"
	metric    "true"
	rollback  "true"
}

step "mkdir_scratch_archive_date", {
	completed "hadoop fs -test -e $HAD_DATASET_DIR"
	command   "hadoop fs -mkdir $HAD_DATASET_DIR"
	metric    "true"
	rollback  "true"
}

// load the mdm_merchants_snapshot data into a local file.
step "load_merchants", {
   completed "test -e $HAD_LANDING_DIR/$HADUKEN_NAME-fletcher-merchants"
   command   executeTemplate("$PIPELINES_CODE_DIR/haduken_v1/load_merchants.sql", 
                "$VSQL_CMD_WARGS -f $TMP_SQL_FILE -A -t > $HAD_LANDING_DIR/$HADUKEN_NAME-fletcher-merchants", 
				[DB_PROD_SCHEMA:DB_PROD_SCHEMA, 
					TXNS_TBL:TXNS_TBL,
					HAD_LANDING_DIR:HAD_LANDING_DIR])
   metric    "wc -l $HAD_LANDING_DIR/$HADUKEN_NAME-fletcher-merchants"
   rollback  "true"
}

// load the factual snapshot table into a local file.
step "load_factual", {
   completed "test -e $HAD_LANDING_DIR/$HADUKEN_NAME-global-places"
   command   executeTemplate("$PIPELINES_CODE_DIR/haduken_v1/load_factual.sql", 
                "$VSQL_CMD_WARGS -f $TMP_SQL_FILE -A -t > $HAD_LANDING_DIR/$HADUKEN_NAME-global-places", 
				[FACTUAL_SCHEMA:FACTUAL_SCHEMA])
   metric    "wc -l $HAD_LANDING_DIR/$HADUKEN_NAME-global-places"
   rollback  "true"
}

// put the merchants data into HDFS.
step "put_merchants",{
   completed "hadoop dfs -test -e $HAD_DATASET_DIR/$HADUKEN_NAME-fletcher-merchants"
   command "hadoop dfs -put $HAD_LANDING_DIR/$HADUKEN_NAME-fletcher-merchants $HAD_DATASET_DIR/$HADUKEN_NAME-fletcher-merchants"
   metric "true" 
   rollback "true"
}

// put the factual data into HDFS.
step "put_factual",{
   completed "hadoop dfs -test -e $HAD_DATASET_DIR/$HADUKEN_NAME-global-places"
   command "hadoop dfs -put $HAD_LANDING_DIR/$HADUKEN_NAME-global-places $HAD_DATASET_DIR/$HADUKEN_NAME-global-places"
   metric "true" 
   rollback "true"
}

// run the Haduken task.
step "run",{ 
   completed "false"
   command   "hadoop jar $PIPELINES_CODE_DIR/haduken_v1/haduken-ent-52314.jar --config $PIPELINES_CODE_DIR/haduken_v1/fletcher_config.py --read_model $PIPELINES_CODE_DIR/haduken_v1/model --confidence 0.2 --project_name factual"
   metric    "true"
   rollback  "true"
}

// copy results from Haduken into a local file.
step "copy",{
   completed "false"
   command   "hadoop dfs -rm -f $HAD_LANDING_DIR/$HADUKEN_NAME-resolutions && hadoop dfs -cat /user/palantir/factual/scoring/*/part-* > $HAD_LANDING_DIR/$HADUKEN_NAME-resolutions"
   metric    "true"
   rollback  "true"
}

// load the results from Haduken into the Factual staging_resolutions_unique table.
step "load_resolutions",{
   completed "false"
   command   executeTemplate("$PIPELINES_CODE_DIR/haduken_v1/load_resolutions.sql", 
	   			"$VSQL_CMD_WARGS -f $TMP_SQL_FILE", 
				[FACTUAL_SCHEMA:FACTUAL_SCHEMA,
					HADUKEN_NAME:HADUKEN_NAME,
					HAD_LANDING_DIR:HAD_LANDING_DIR])
   metric    "true"
   rollback  "true"
}

// clean up any incidental local files created by Haduken.
step "clean_temp_files",{
   completed "false"
   command   "rm $HAD_LANDING_DIR/$HADUKEN_NAME*" 
   metric    "true"
   rollback  "true"
}

// clean up any incidental HDFS files created by Haduken.
step "clean_hdfs_files",{
   completed "false"
   command   "hadoop dfs -rm $HAD_DATASET_DIR/$HADUKEN_NAME*" 
   metric    "true"
   rollback  "true"
}

// clean up any incidental output files created by Haduken.
step "clean_haduken_output_files",{
   completed "false"
   command   "hadoop dfs -rmr /user/palantir/factual" 
   metric    "true"
   rollback  "true"
}


// Load haduken results into the Vertica factual_resolutions table.
step "load_join_table",{
    completed "false"
    command   executeTemplate("$PIPELINES_CODE_DIR/factual_load_join.sql",
                "$VSQL_CMD_WARGS -f $TMP_SQL_FILE", 
				[FACTUAL_SCHEMA:FACTUAL_SCHEMA])
    //metric    "echo " + joinTableRows(ENV)
    metric "true"
    rollback  "true"
}

// Run stats on factual tables
step "run_stats",{
    completed "false"
    command   executeTemplate("$PIPELINES_CODE_DIR/run_stats_on_factual.sql",
                "$VSQL_CMD_WARGS -f $TMP_SQL_FILE", 
        [FACTUAL_SCHEMA:FACTUAL_SCHEMA])
    metric "true"
    rollback  "true"
}

// cleanup steps

// tar.gz the scratch directory and delete the original.
step "tar_gz_scratch_dir", {
    completed "false"
    command   """   cd $SCRATCH_DIR_FULL/.. && 
                    tar -cvzf ${SCRATCH_DIR}.tar.gz $SCRATCH_DIR && 
                    rm -r $SCRATCH_DIR """
    metric    "true"
    rollback  "true"
}

// make a Hadoop directory to archive logs for this job.
step "mkdir_logs_archive_date", {
    completed "hadoop fs -test -e $HDFS_LOGS_ARCHIVE_DIR"
    command   "hadoop fs -mkdir $HDFS_LOGS_ARCHIVE_DIR"
    metric    "true"
    rollback  "true"
}

// remove any existing logs for this job in Hadoop.
step "rm_archived_logs", {
   completed  "! hadoop fs -test -e $HDFS_LOGS_ARCHIVE_DIR/${SCRATCH_DIR}.tar.gz"
   command    "hadoop fs -rm $HDFS_LOGS_ARCHIVE_DIR/${SCRATCH_DIR}.tar.gz"
   metric     "true"
   rollback   "true"
}

// copy the logs into Hadoop for storage.
step "archive_logs", {
   completed  "false"
   command    """   hadoop fs -put ${SCRATCH_DIR_FULL}.tar.gz $HDFS_LOGS_ARCHIVE_DIR &&
                    rm ${SCRATCH_DIR_FULL}.tar.gz  """
   metric     "true"
   rollback   "true"
}

