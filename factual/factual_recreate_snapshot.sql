\timing
DROP TABLE IF EXISTS $FACTUAL_SCHEMA.factual_snapshot_staging;

CREATE TABLE $FACTUAL_SCHEMA.factual_snapshot_staging 
	LIKE $DB_RAW_SCHEMA.$FACTUAL_VERSIONED_TBL INCLUDING PROJECTIONS;


INSERT INTO $FACTUAL_SCHEMA.factual_snapshot_staging
SELECT
   t1.factual_id as factual_id,
   t1.name as name,
   t1.address as address,
   t1.address_extended as address_extended,
   t1.po_box as po_box,
   t1.locality as locality,
   t1.region as region,
   t1.post_town as post_town,
   t1.admin_region as admin_region,
   t1.postcode as postcode,
   t1.country as country,
   t1.tel as tel,
   t1.fax as fax,
   t1.neighborhood as neighborhood,
   t1.website as website,
   t1.email as email,
   t1.category_ids as category_ids,
   t1.category_labels as category_labels,
   t1.chain_name as chain_name,
   t1.chain_id as chain_id,
   t1.hours as hours,
   t1.hours_display as hours_display,
   t1.existence as existence,
   t1.cuisine as cuisine,
   t1.price as price,
   t1.rating as rating,
   t1.payment_cashonly as payment_cashonly,
   t1.reservations as reservations,
   t1.open_24hrs as open_24hrs,
   t1.founded as founded,
   t1.attire as attire,
   t1.attire_required as attire_required,
   t1.attire_prohibited as attire_prohibited,
   t1.parking as parking,
   t1.parking_valet as parking_valet,
   t1.parking_garage as parking_garage,
   t1.parking_street as parking_street,
   t1.parking_lot as parking_lot,
   t1.parking_validated as parking_validated,
   t1.parking_free as parking_free,
   t1.smoking as smoking,
   t1.meal_breakfast as meal_breakfast,
   t1.meal_lunch as meal_lunch,
   t1.meal_dinner as meal_dinner,
   t1.meal_deliver as meal_deliver,
   t1.meal_takeout as meal_takeout,
   t1.meal_cater as meal_cater,
   t1.alcohol as alcohol,
   t1.alcohol_bar as alcohol_bar,
   t1.alcohol_beer_wine as alcohol_beer_wine,
   t1.alcohol_byob as alcohol_byob,
   t1.kids_goodfor as kids_goodfor,
   t1.kids_menu as kids_menu,
   t1.groups_goodfor as groups_goodfor,
   t1.accessible_wheelchair as accessible_wheelchair,
   t1.seating_outdoor as seating_outdoor,
   t1.wifi as wifi,
   t1.owner as owner,
   t1.room_private as room_private,
   t1.options_vegetarian as options_vegetarian,
   t1.options_vegan as options_vegan,
   t1.options_glutenfree as options_glutenfree,
   t1.options_lowfat as options_lowfat,
   t1.options_organic as options_organic,
   t1.options_healthy as options_healthy,
   t1.update_date as update_date,
   t1.origin_source_date as origin_source_date,
   t1.origin_source_file as origin_source_file
FROM (
    SELECT
        factual_id,
        LAST_VALUE (name  IGNORE NULLS)
                    OVER ( PARTITION BY factual_id ORDER BY update_date ASC, origin_source_date ASC )
                    AS name ,
        LAST_VALUE (address  IGNORE NULLS)
                    OVER ( PARTITION BY factual_id ORDER BY update_date ASC, origin_source_date ASC )
                    AS address ,
        LAST_VALUE (address_extended  IGNORE NULLS)
                    OVER ( PARTITION BY factual_id ORDER BY update_date ASC, origin_source_date ASC )
                    AS address_extended ,
        LAST_VALUE (po_box  IGNORE NULLS)
                    OVER ( PARTITION BY factual_id ORDER BY update_date ASC, origin_source_date ASC )
                    AS po_box ,
        LAST_VALUE (locality  IGNORE NULLS)
                    OVER ( PARTITION BY factual_id ORDER BY update_date ASC, origin_source_date ASC )
                    AS locality ,
        LAST_VALUE (region  IGNORE NULLS)
                    OVER ( PARTITION BY factual_id ORDER BY update_date ASC, origin_source_date ASC )
                    AS region ,
        LAST_VALUE (post_town  IGNORE NULLS)
                    OVER ( PARTITION BY factual_id ORDER BY update_date ASC, origin_source_date ASC )
                    AS post_town ,
        LAST_VALUE (admin_region  IGNORE NULLS)
                    OVER ( PARTITION BY factual_id ORDER BY update_date ASC, origin_source_date ASC )
                    AS admin_region ,
        LAST_VALUE (postcode  IGNORE NULLS)
                    OVER ( PARTITION BY factual_id ORDER BY update_date ASC, origin_source_date ASC )
                    AS postcode ,
        LAST_VALUE (country  IGNORE NULLS)
                    OVER ( PARTITION BY factual_id ORDER BY update_date ASC, origin_source_date ASC )
                    AS country ,
        LAST_VALUE (tel  IGNORE NULLS)
                    OVER ( PARTITION BY factual_id ORDER BY update_date ASC, origin_source_date ASC )
                    AS tel ,
        LAST_VALUE (fax  IGNORE NULLS)
                    OVER ( PARTITION BY factual_id ORDER BY update_date ASC, origin_source_date ASC )
                    AS fax ,
        LAST_VALUE (neighborhood  IGNORE NULLS)
                    OVER ( PARTITION BY factual_id ORDER BY update_date ASC, origin_source_date ASC )
                    AS neighborhood ,
        LAST_VALUE (website  IGNORE NULLS)
                    OVER ( PARTITION BY factual_id ORDER BY update_date ASC, origin_source_date ASC )
                    AS website ,
        LAST_VALUE (email  IGNORE NULLS)
                    OVER ( PARTITION BY factual_id ORDER BY update_date ASC, origin_source_date ASC )
                    AS email ,
        LAST_VALUE (category_ids  IGNORE NULLS)
                    OVER ( PARTITION BY factual_id ORDER BY update_date ASC, origin_source_date ASC )
                    AS category_ids ,
        LAST_VALUE (category_labels  IGNORE NULLS)
                    OVER ( PARTITION BY factual_id ORDER BY update_date ASC, origin_source_date ASC )
                    AS category_labels ,
        LAST_VALUE (chain_name  IGNORE NULLS)
                    OVER ( PARTITION BY factual_id ORDER BY update_date ASC, origin_source_date ASC )
                    AS chain_name ,
        LAST_VALUE (chain_id  IGNORE NULLS)
                    OVER ( PARTITION BY factual_id ORDER BY update_date ASC, origin_source_date ASC )
                    AS chain_id ,
        LAST_VALUE (hours  IGNORE NULLS)
                    OVER ( PARTITION BY factual_id ORDER BY update_date ASC, origin_source_date ASC )
                    AS hours ,
        LAST_VALUE (hours_display  IGNORE NULLS)
                    OVER ( PARTITION BY factual_id ORDER BY update_date ASC, origin_source_date ASC )
                    AS hours_display ,
        LAST_VALUE (existence  IGNORE NULLS)
                    OVER ( PARTITION BY factual_id ORDER BY update_date ASC, origin_source_date ASC )
                    AS existence ,
        LAST_VALUE (cuisine  IGNORE NULLS)
                    OVER ( PARTITION BY factual_id ORDER BY update_date ASC, origin_source_date ASC )
                    AS cuisine ,
        LAST_VALUE (price  IGNORE NULLS)
                    OVER ( PARTITION BY factual_id ORDER BY update_date ASC, origin_source_date ASC )
                    AS price ,
        LAST_VALUE (rating  IGNORE NULLS)
                    OVER ( PARTITION BY factual_id ORDER BY update_date ASC, origin_source_date ASC )
                    AS rating ,
        LAST_VALUE (payment_cashonly IGNORE NULLS)
                    OVER ( PARTITION BY factual_id ORDER BY update_date ASC, origin_source_date ASC )
                    AS payment_cashonly,
        LAST_VALUE (reservations  IGNORE NULLS)
                    OVER ( PARTITION BY factual_id ORDER BY update_date ASC, origin_source_date ASC )
                    AS reservations ,
        LAST_VALUE (open_24hrs  IGNORE NULLS)
                    OVER ( PARTITION BY factual_id ORDER BY update_date ASC, origin_source_date ASC )
                    AS open_24hrs ,
        LAST_VALUE (founded  IGNORE NULLS)
                    OVER ( PARTITION BY factual_id ORDER BY update_date ASC, origin_source_date ASC )
                    AS founded ,
        LAST_VALUE (attire  IGNORE NULLS)
                    OVER ( PARTITION BY factual_id ORDER BY update_date ASC, origin_source_date ASC )
                    AS attire ,
        LAST_VALUE (attire_required  IGNORE NULLS)
                    OVER ( PARTITION BY factual_id ORDER BY update_date ASC, origin_source_date ASC )
                    AS attire_required ,
        LAST_VALUE (attire_prohibited  IGNORE NULLS)
                    OVER ( PARTITION BY factual_id ORDER BY update_date ASC, origin_source_date ASC )
                    AS attire_prohibited ,
        LAST_VALUE (parking  IGNORE NULLS)
                    OVER ( PARTITION BY factual_id ORDER BY update_date ASC, origin_source_date ASC )
                    AS parking ,
        LAST_VALUE (parking_valet  IGNORE NULLS)
                    OVER ( PARTITION BY factual_id ORDER BY update_date ASC, origin_source_date ASC )
                    AS parking_valet ,
        LAST_VALUE (parking_garage  IGNORE NULLS)
                    OVER ( PARTITION BY factual_id ORDER BY update_date ASC, origin_source_date ASC )
                    AS parking_garage ,
        LAST_VALUE (parking_street  IGNORE NULLS)
                    OVER ( PARTITION BY factual_id ORDER BY update_date ASC, origin_source_date ASC )
                    AS parking_street ,
        LAST_VALUE (parking_lot  IGNORE NULLS)
                    OVER ( PARTITION BY factual_id ORDER BY update_date ASC, origin_source_date ASC )
                    AS parking_lot ,
        LAST_VALUE (parking_validated  IGNORE NULLS)
                    OVER ( PARTITION BY factual_id ORDER BY update_date ASC, origin_source_date ASC )
                    AS parking_validated ,
        LAST_VALUE (parking_free  IGNORE NULLS)
                    OVER ( PARTITION BY factual_id ORDER BY update_date ASC, origin_source_date ASC )
                    AS parking_free ,
        LAST_VALUE (smoking  IGNORE NULLS)
                    OVER ( PARTITION BY factual_id ORDER BY update_date ASC, origin_source_date ASC )
                    AS smoking ,
        LAST_VALUE (meal_breakfast  IGNORE NULLS)
                    OVER ( PARTITION BY factual_id ORDER BY update_date ASC, origin_source_date ASC )
                    AS meal_breakfast ,
        LAST_VALUE (meal_lunch  IGNORE NULLS)
                    OVER ( PARTITION BY factual_id ORDER BY update_date ASC, origin_source_date ASC )
                    AS meal_lunch ,
        LAST_VALUE (meal_dinner  IGNORE NULLS)
                    OVER ( PARTITION BY factual_id ORDER BY update_date ASC, origin_source_date ASC )
                    AS meal_dinner ,
        LAST_VALUE (meal_deliver  IGNORE NULLS)
                    OVER ( PARTITION BY factual_id ORDER BY update_date ASC, origin_source_date ASC )
                    AS meal_deliver ,
        LAST_VALUE (meal_takeout  IGNORE NULLS)
                    OVER ( PARTITION BY factual_id ORDER BY update_date ASC, origin_source_date ASC )
                    AS meal_takeout ,
        LAST_VALUE (meal_cater  IGNORE NULLS)
                    OVER ( PARTITION BY factual_id ORDER BY update_date ASC, origin_source_date ASC )
                    AS meal_cater ,
        LAST_VALUE (alcohol  IGNORE NULLS)
                    OVER ( PARTITION BY factual_id ORDER BY update_date ASC, origin_source_date ASC )
                    AS alcohol ,
        LAST_VALUE (alcohol_bar  IGNORE NULLS)
                    OVER ( PARTITION BY factual_id ORDER BY update_date ASC, origin_source_date ASC )
                    AS alcohol_bar ,
        LAST_VALUE (alcohol_beer_wine  IGNORE NULLS)
                    OVER ( PARTITION BY factual_id ORDER BY update_date ASC, origin_source_date ASC )
                    AS alcohol_beer_wine ,
        LAST_VALUE (alcohol_byob  IGNORE NULLS)
                    OVER ( PARTITION BY factual_id ORDER BY update_date ASC, origin_source_date ASC )
                    AS alcohol_byob ,
        LAST_VALUE (kids_goodfor  IGNORE NULLS)
                    OVER ( PARTITION BY factual_id ORDER BY update_date ASC, origin_source_date ASC )
                    AS kids_goodfor ,
        LAST_VALUE (kids_menu  IGNORE NULLS)
                    OVER ( PARTITION BY factual_id ORDER BY update_date ASC, origin_source_date ASC )
                    AS kids_menu ,
        LAST_VALUE (groups_goodfor  IGNORE NULLS)
                    OVER ( PARTITION BY factual_id ORDER BY update_date ASC, origin_source_date ASC )
                    AS groups_goodfor ,
        LAST_VALUE (accessible_wheelchair  IGNORE NULLS)
                    OVER ( PARTITION BY factual_id ORDER BY update_date ASC, origin_source_date ASC )
                    AS accessible_wheelchair ,
        LAST_VALUE (seating_outdoor  IGNORE NULLS)
                    OVER ( PARTITION BY factual_id ORDER BY update_date ASC, origin_source_date ASC )
                    AS seating_outdoor ,
        LAST_VALUE (wifi  IGNORE NULLS)
                    OVER ( PARTITION BY factual_id ORDER BY update_date ASC, origin_source_date ASC )
                    AS wifi ,
        LAST_VALUE (owner  IGNORE NULLS)
                    OVER ( PARTITION BY factual_id ORDER BY update_date ASC, origin_source_date ASC )
                    AS owner ,
        LAST_VALUE (room_private IGNORE NULLS)
                    OVER ( PARTITION BY factual_id ORDER BY update_date ASC, origin_source_date ASC )
                    AS room_private,
        LAST_VALUE (options_vegetarian IGNORE NULLS)
                    OVER ( PARTITION BY factual_id ORDER BY update_date ASC, origin_source_date ASC )
                    AS options_vegetarian,
        LAST_VALUE (options_vegan IGNORE NULLS)
                    OVER ( PARTITION BY factual_id ORDER BY update_date ASC, origin_source_date ASC )
                    AS options_vegan,
        LAST_VALUE (options_glutenfree IGNORE NULLS)
                    OVER ( PARTITION BY factual_id ORDER BY update_date ASC, origin_source_date ASC )
                    AS options_glutenfree,
        LAST_VALUE (options_lowfat IGNORE NULLS)
                    OVER ( PARTITION BY factual_id ORDER BY update_date ASC, origin_source_date ASC )
                    AS options_lowfat,
        LAST_VALUE (options_organic IGNORE NULLS)
                    OVER ( PARTITION BY factual_id ORDER BY update_date ASC, origin_source_date ASC )
                    AS options_organic,
        LAST_VALUE (options_healthy IGNORE NULLS)
                    OVER ( PARTITION BY factual_id ORDER BY update_date ASC, origin_source_date ASC )
                    AS options_healthy,
        LAST_VALUE (update_date IGNORE NULLS)
                    OVER ( PARTITION BY factual_id ORDER BY update_date ASC, origin_source_date ASC )
                    AS update_date,
        LAST_VALUE (origin_source_date IGNORE NULLS)
                    OVER ( PARTITION BY factual_id ORDER BY update_date ASC, origin_source_date ASC )
                    AS origin_source_date,
        LAST_VALUE (origin_source_file IGNORE NULLS)
                    OVER ( PARTITION BY factual_id ORDER BY update_date ASC, origin_source_date ASC )
                    AS origin_source_file,                    
        ROW_NUMBER()
                    OVER (PARTITION BY factual_id ORDER BY update_date DESC, origin_source_date DESC)
                    as row_num

    FROM
        $DB_RAW_SCHEMA.$FACTUAL_VERSIONED_TBL
) t1
WHERE row_num = 1
;

-- this is an ugly way to throw an error 
SELECT (1/(SELECT COUNT(*) FROM $FACTUAL_SCHEMA.factual_snapshot_staging) );

TRUNCATE TABLE $FACTUAL_SCHEMA.factual_snapshot;

INSERT INTO $FACTUAL_SCHEMA.factual_snapshot select * from $FACTUAL_SCHEMA.factual_snapshot_staging;

COMMIT;


GRANT SELECT ON $FACTUAL_SCHEMA.factual_snapshot to valhalla_prod_read_role;
GRANT SELECT ON $FACTUAL_SCHEMA.factual_snapshot TO nationals_admin_role WITH GRANT OPTION;
GRANT SELECT ON $FACTUAL_SCHEMA.factual_snapshot TO nationals_bams_role;
GRANT SELECT ON $FACTUAL_SCHEMA.factual_snapshot TO nationals_fd_role;
GRANT SELECT ON $FACTUAL_SCHEMA.factual_snapshot TO fd_fd_role;
GRANT SELECT ON $FACTUAL_SCHEMA.factual_snapshot TO fd_admin_role;
GRANT SELECT ON $FACTUAL_SCHEMA.factual_snapshot TO fd_bams_role;

select analyze_histogram('$FACTUAL_SCHEMA.factual_snapshot', 100);
