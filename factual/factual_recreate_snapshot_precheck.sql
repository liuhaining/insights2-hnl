-- factual_recreate_snapshot_precheck.sql
-- return TRUE if the recreate snapshot should run, e.g. max timestamp from versioned > max timestamp from snapshot

select max(ver_max) > max(snap_max) 
from (
    select max(cast(update_date as integer)) as ver_max, 0 as snap_max 
      from raw_:env.factual_versioned
    union all
    select 0 as ver_max, max(cast(update_date as integer)) as snap_max 
      from factual_:env.factual_snapshot
) t1;