#!/bin/bash
# A wrapper script for running Haduken insights on Factual data and then
# recreating the Factual snapshot table.
#
# Usage: sh factual_recreate_snapshot_wrapper.sh [ENV] [SEND_REPORT] [SEND_TO]
#   SEND_REPORT     : (optional) whether to send the DQ report - will only send if SEND_REPORT=true
#   SEND_TO         : (optional) e-mail address to whom the report will be sent

# turn this on for full debugging info
set -x

# error out script if any command fails
set -e

# Determine current working directory based on this script's location
export CWD=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
# Common directory
COMMON_DIR=${CWD}/../../common
SENDREPORT_JAR=/data/jars/prod/genericEmailSender.jar 

# This script's name
NAME="factual_recreate_snapshot"
DESCRIPTION="A wrapper script for running the Factual recreate snapshot command. Generates Haduken merchant matches between Factual and Fletcher IDs."
# Groovy Script this wrapper executes
MATILDA_SCRIPT="${CWD}/factual_recreate_snapshot.groovy"


# These functions must be declared prior to call to init.sh
# This function just processes/parses extra arguments passed with -o
function process_local_arguments {
     printf "Executing local process_local_arguments with arguments $OPTIONAL_ARGS\n"
    
    # Parse the string into array
    IFS=' ' read -a ARG_ARRAY <<< "$OPTIONAL_ARGS"

 	# Send report
    SEND_REPORT=false

    if [ ! -z "${ARG_ARRAY[0]// }" ]; then
        SEND_REPORT="${ARG_ARRAY[0]}" 
    fi

    echo "Value of SEND_REPORT is ${SEND_REPORT}"

    # Send To
        
    if [ ! -z "${ARG_ARRAY[1]// }" ]; then
        SEND_TO="${ARG_ARRAY[1]}" 
    fi

    echo "Value of SEND_TO is ${SEND_TO}"

    printf "Running Factual-Recreate-Snapshot with following parameters: \n"
    printf "\tEnvironment   = ${ENV}\n"
    printf "\tEmail         = ${SEND_TO}\n"
    printf "\tSend Email         = ${SEND_REPORT}\n"

}

# This function shows additional help content
function show_more_help {
    printf ' \t -o other arguments to be passed to the matilda script \n'
    printf ' \n\t arguments are as follows\n'
    printf ' \t\t -o "[[SEND_REPORT] [SEND_TO]"\n\n'
    printf ' \t\t SEND_REPORT : (optional) whether to send the DQ report - will only send if SEND_REPORT=true\n'
    printf ' \t\t SEND_TO     : (optional) e-mail address to whom the report will be sent\n'
}

# This function shows alternative switches used with this script only in the "help" menu
# This function is automatically called(if exists) by init.sh when __show_help() is invoked
function show_alt_synopsis {
    printf ' -o "[SEND_REPORT] [SEND_TO]"'
}

# Process arguments, source environment
source $COMMON_DIR/init.sh

# Log file for this process - these statements must occur after sourcing init.sh
# env-$ENV.sh declares $LOCAL_LOG_LOC
DATE=$(date "+%Y%m%d")
LOGNAME="${NAME}.${DATE}.log"
LOCAL_LOG="${LOCAL_LOG_LOC}/${LOGNAME}"
HDFS_LOG="s3e://pcloud-valhalla-pipeline-maroon/log/${NAME}/${LOGNAME}"
__announcement

if [ ! -d "${LOCAL_LOG_LOC}/${NAME}/" ]; then 
    mkdir -p ${LOCAL_LOG_LOC}/${NAME}/
fi             

# Check if JAVA_HOME is defined (in init.sh)
__check_java

# Fire it up. $MATILDA_LIB comes in from env-$ENV.sh being sourced (initialized by) init.sh

# # execute pipeline, appending splitting output to log
cd /data/landing/scratch_space # so that any random Hive files get written here


# If a new Restaurants and Places file both have not been processed since a Recreate Snapshot 
# job has run, then the script should exit.
SHOULDRUN=$($DB_CONNECT -At -v env=$ENV -f "${CWD}/factual_recreate_snapshot_precheck.sql");

if [ "$SHOULDRUN" == 'f' ]
then
    exit 0
fi

${JAVA_HOME}/bin/java ${MATILDA_JAVA_OPTS} -jar ${MATILDA_LIB} ${MATILDA_SCRIPT_OPTS} --script ${MATILDA_SCRIPT} |& tee -a $LOCAL_LOG

if [ "$SEND_REPORT" == "true" ]
then
# If the job ran successfully, then send a confirmation email.
java -jar $SENDREPORT_JAR "Palantir: New Factual resolutions are available for download - " "$SEND_TO" "Notification: The latest Factual files have been processed in the Palantir system. New Factual resolutions are also available for download. Thank you!" -t
fi
