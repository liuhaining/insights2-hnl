from itertools import product
from cascading.tap.hadoop import Hfs

fletcher_fields = ["fletcher_id", "fletcher_name_1", "fletcher_name_2", "fletcher_name_3", "fletcher_address", "fletcher_zip", "fletcher_tel"]
factual_fields = ["factual_id", "factual_name", "factual_address", "factual_zip", "factual_tel"]

HadukenConfig.right_input = Hfs(TextDelimited(Fields(fletcher_fields), True, '|'), 'datasets/staging-fletcher-merchants')
HadukenConfig.right_input_fields = fletcher_fields

HadukenConfig.left_input = Hfs(TextDelimited(Fields(factual_fields), True, '|'), 'datasets/staging-global-places')
HadukenConfig.left_input_fields = factual_fields

HadukenConfig.id_fields = ["fletcher_id", "factual_id"]
HadukenConfig.readable_output_fields = ["fletcher_name_1", "fletcher_name_2", "fletcher_name_3", "fletcher_address", "fletcher_tel", "factual_name", "factual_address", "factual_tel"]


## Cleaners
right_names = ["fletcher_name_1", "fletcher_name_2", "fletcher_name_3", "fletcher_address"]
left_names = ["factual_name", "factual_address"]

downcase = LowerCaseNormalizeCleaner()
remove_apostrophes = RegexWiper("[']+", "")
remove_punctuation = RegexWiper("[^0-9a-zA-Z \-']+"," ")
dedup_spaces = RegexWiper("\\s+"," ")
stop_words = StopWordCleaner(["inc", "the", "llc", "company", "restaurant", "co", "cafe", "caffe", "corp", "grp", "div", "ret", "and", "of", "md"])
digits_only = DigitsOnlyCleaner()

name_cleaners = [downcase, remove_apostrophes, remove_punctuation, dedup_spaces, stop_words]

right_cleaned_fields, right_cleaners = zip(*product(right_names, name_cleaners))
left_cleaned_fields, left_cleaners = zip(*product(left_names, name_cleaners))


rc = [x for x in right_cleaners]
rc.append(digits_only)

rcf = [x for x in right_cleaned_fields]
rcf.append("fletcher_tel")

lc = [x for x in left_cleaners]
lc.append(digits_only)

lcf = [x for x in left_cleaned_fields]
lcf.append("factual_tel")

HadukenConfig.right_cleaners = rc
HadukenConfig.left_cleaners = lc
HadukenConfig.right_fields = rcf
HadukenConfig.left_fields = lcf

## Blockers
## Blockers

# HadukenConfig.right_blocker = SubstringBlocker("fletcher_address", 10) # RegexBlocker('fletcher_address', '[a-zA-Z0-9][a-zA-Z0-9\-]+[a-zA-Z]')

# HadukenConfig.right_blocker = AndBlocker(LabelBlocker('fletcher_zip'), RegexBlocker('fletcher_address', '[a-zA-Z0-9][a-zA-Z0-9\-]+[a-zA-Z]'))

# HadukenConfig.right_blocker = SubstringBlocker("fletcher_zip", 4)
# HadukenConfig.left_blocker = SubstringBlocker("factual_zip", 4)

HadukenConfig.right_blocker = OrBlocker(AndBlocker(LabelBlocker('fletcher_zip'), AndBlocker(RegexBlocker('fletcher_address', '[0-9\-]+'), RegexBlocker('fletcher_address', '[a-zA-Z0-9][a-zA-Z0-9\-]+[a-zA-Z]'))),
  AndBlocker(LabelBlocker('fletcher_zip'), LabelBlockerExcept("fletcher_tel", ["0000000000", "1111111111", "2222222222", "3333333333", "4444444444", "5555555555", "6666666666", "7777777777", "8888888888", "9999999999"])))

HadukenConfig.left_blocker = OrBlocker(AndBlocker(LabelBlocker('factual_zip'), AndBlocker(RegexBlocker('factual_address', '[0-9\-]+'), RegexBlocker('factual_address', '[a-zA-Z0-9][a-zA-Z0-9\-]+[a-zA-Z]'))),
  AndBlocker(LabelBlocker('factual_zip'), LabelBlockerExcept("factual_tel", ["0000000000", "1111111111", "2222222222", "3333333333", "4444444444", "5555555555", "6666666666", "7777777777", "8888888888", "9999999999"])))

HadukenConfig.batch_label_count = 10000

## Comparators
absDiff = AbsoluteDifferenceComparator()
qgram = QGramComparator(QGramComparator.Formula.JACCARD, QGramComparator.Tokenizer.BASIC, 3)
jw = JaroWinkler()
jaccard = JaccardIndexComparator()
subtokens = MatchingTokensComparator()
cc = ComparatorConfig()

for p in product(fletcher_fields[1:], factual_fields[1:]):
    cc.addComparator(qgram, p[0], p[1], "qgram_"+p[0]+"_"+p[1])
    cc.addComparator(jw, p[0], p[1], "jw" + "_" + p[0] + "_" + p[1])
    cc.addComparator(jaccard, p[0], p[1], "jc_" + "_" + p[0] + "_" + p[1])
    cc.addComparator(subtokens, p[0], p[1], "subtokens_" + p[0] + "_" + p[1])

HadukenConfig.compare_config = cc

HadukenConfig.batch_label_count = 10000
HadukenConfig.inner_batch_size = 1000
HadukenConfig.sampling_rate = 0.0005
