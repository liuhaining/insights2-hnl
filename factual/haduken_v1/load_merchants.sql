select 
    (a.merchant_id || '_' || a.platform_id), 
    REPLACE(a.merchant_name,'|',''), 
    REPLACE(a.billing_name,'|',''), 
    REPLACE(a.outlet_legal_name,'|',''), 
    REPLACE(a.address_dba_first_line_text,'|',''),
    a.address_dba_count_code, 
    COALESCE(COALESCE(a.contact_dba_telephone_number,a.contact_blln_telephone_number),a.contact_cust_supp_telephone_number) 
from $DB_PROD_SCHEMA.mdm_merchants_snapshot a
where exists (select true from $DB_PROD_SCHEMA.$TXNS_TBL where a.platform_id = platform_id and a.merchant_id = merchant_id);
