drop table if exists $FACTUAL_SCHEMA.$HADUKEN_NAME_resolutions;

drop table if exists $FACTUAL_SCHEMA.$HADUKEN_NAME_resolutions_max_conf;

drop table if exists $FACTUAL_SCHEMA.$HADUKEN_NAME_resolutions_unique;

create table $FACTUAL_SCHEMA.$HADUKEN_NAME_resolutions (merchant_id varchar, factual_id varchar, confidence float);

copy $FACTUAL_SCHEMA.$HADUKEN_NAME_resolutions from local '$HAD_LANDING_DIR/$HADUKEN_NAME-resolutions'  delimiter '|' null '' abort on error;

create table $FACTUAL_SCHEMA.$HADUKEN_NAME_resolutions_max_conf (merchant_id varchar, factual_id varchar, confidence float);

insert into $FACTUAL_SCHEMA.$HADUKEN_NAME_resolutions_max_conf select t.merchant_id, t.factual_id, t.confidence from $FACTUAL_SCHEMA.$HADUKEN_NAME_resolutions t
	join (select merchant_id, max(confidence) as max_conf from $FACTUAL_SCHEMA.$HADUKEN_NAME_resolutions group by merchant_id) q 
	on t.merchant_id = q.merchant_id and t.confidence = q.max_conf; 

create table $FACTUAL_SCHEMA.$HADUKEN_NAME_resolutions_unique (merchant_id varchar, factual_id varchar, confidence float);

insert into $FACTUAL_SCHEMA.$HADUKEN_NAME_resolutions_unique select t.merchant_id, t.factual_id, t.confidence from $FACTUAL_SCHEMA.$HADUKEN_NAME_resolutions_max_conf t 
	join (select merchant_id, max(factual_id) as max_id from $FACTUAL_SCHEMA.$HADUKEN_NAME_resolutions_max_conf group by merchant_id) q 
	on t.merchant_id = q.merchant_id and t.factual_id = q.max_id; 

create table $FACTUAL_SCHEMA.temp_unique as SELECT SPLIT_PART(merchant_id,'_',1) as merchant_id, SPLIT_PART(merchant_id,'_',2) as platform_id, factual_id, confidence
from $FACTUAL_SCHEMA.staging_resolutions_unique where confidence > 0.5;

drop table $FACTUAL_SCHEMA.staging_resolutions_unique;

alter table $FACTUAL_SCHEMA.temp_unique rename to staging_resolutions_unique;

COMMIT;
