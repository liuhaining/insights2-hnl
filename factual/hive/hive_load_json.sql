USE $HIVE_SCHEMA;

-- drop partition if it exists
ALTER TABLE $LANDING_JSON_TBL DROP IF EXISTS PARTITION (
    source_date = '$SOURCE_DATE',
    source_cat = '$SOURCE_CAT', 
    source_file = '$SOURCE_FILE'
);

-- create partition
ALTER TABLE $LANDING_JSON_TBL ADD IF NOT EXISTS PARTITION (
    source_date = '$SOURCE_DATE',
    source_cat = '$SOURCE_CAT', 
    source_file = '$SOURCE_FILE'
);

-- copy files from DIS landing location to partiion
dfs -put $SCRATCH_DIR_FULL_GZ $LANDING_JSON_PART_FP/;

