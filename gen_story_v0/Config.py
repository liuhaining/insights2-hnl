import ConfigParser
import logging.config
import re
logging.config.fileConfig('config/log_config.ini')
LOG = logging.getLogger('story_generator')

def load_config(filename):
    config = {}
    Config = ConfigParser.SafeConfigParser()
    Config.read(filename)

    LOG.info("config section = %s", Config.sections())
    config['metrics_host'] = Config.get("metrics_db", "host")
    config['metrics_port'] = Config.get("metrics_db", "port")
    config['metrics_db_name'] = Config.get("metrics_db", "name")
    config['metrics_user'] = Config.get("metrics_db", "user")
    config['metrics_password'] = Config.get("metrics_db", "password")

    config['vhost'] = Config.get("story_db", "host")
    config['vname'] = Config.get("story_db", "name")
    config['vuser'] = Config.get("story_db", "user")
    config['vpassword'] = Config.get("story_db", "password")

    # config['cohort_by_mcc'] = Config.get("sql", "cohort_by_mcc")


    LOG.debug("cohort config =%s", config)
    return config


def read_sql_file(filename):
    with open(filename) as fp:
        lines = fp.read().splitlines()
        return re.sub(r'\s+', ' ', ' '.join(lines))
