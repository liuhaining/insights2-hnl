# Design

We want to 
- load all `metrics` dataset
- if a day passed and there are new data, just load and update in-memory
- if data are unable from `metrics` and join them if necessary
- transform JSON objects and flat them out
- create dataframe (DF) and regularly persist into disk as snapshot backup
- perform all story generations on DF 
- update stories to `story` table

# Verification
## Daily checks
- `aireflow` checks data quality
- `airflow` regularly checks no missing stories

## Story re-gen

## Cohort or similar business selection
https://gyftteam.atlassian.net/wiki/spaces/IN/pages/447873025/Similar+business+selection

