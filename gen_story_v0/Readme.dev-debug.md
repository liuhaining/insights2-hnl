# Setup
- Install Spark locally
- Run Spark shell, we use python
    
    `spark-shell`
    
    `$ which pyspark`
     
     `/usr/local/bin/pyspark`
   
## Run
Run spark-submit,
a sample run is: https://github.com/Soluto/spark-submit-with-pyspark-template
`spark-submit SimpleApp.py --some_arg=some_value --pip_modules=pip_modules.jar --src=src.jar`

## Debug
`export SPARK_SUBMIT_OPTS=-agentlib:jdwp=transport=dt_socket,server=y,suspend=y,address=5005`

`$ spark-submit run-example SparkTC 1
 Listening for transport dt_socket at address: 5005`

    