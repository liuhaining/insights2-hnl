#!/usr/bin/env bash

echo "run at pushserver ..."

OUTPUT=/data/fd-data/smb_info_2018.csv

/opt/vertica/bin/vsql -h 10.161.56.241 Valhalla f3b8i2z_liu_fd -wchangeme \
-F $'^' -At -o $OUTPUT \
-c "
create local temp table smb_tmp_mid_pid on commit preserve rows as
    select t.merchant_id,  t.platform_id, t.tkt as ticket
    from
        (SELECT  merchant_id,  platform_id,sum(txn_amt) as total,  avg(txn_amt_avg)  as tkt FROM fletcher_prod.txns_agg_by_mid_mdate_unfiltered
        where merchant_txn_date between '2018-01-01' and '2018-12-31'
        group by 1,2 ) t
    where   t.total<10000000
    group by 1,2,3;

select tx.merchant_id as mid, tx.platform_id as pid, tx.ticket,
    mdm.merchant_mcc as mcc,
    mdm.address_dba_country_subdivision_code as state,
    substr(mdm.address_dba_count_code, 1,5) as zip_str,
    mdm.address_dba_lat as lat, mdm.address_dba_lon as lon
from fletcher_prod.mdm_merchants_snapshot mdm
    join smb_tmp_mid_pid tx
        on tx.platform_id=mdm.platform_id and tx.merchant_id=mdm.merchant_id
    where (address_dba_country_code like 'US%' or address_dba_country_code like '840' )
    and merchant_mcc is not null

"
wc -l $OUTPUT
gzip $OUTPUT