#!/usr/bin/env bash

echo "run at pushserver ..."

OUTPUT=/data/fd-data/smb_mids.csv

/opt/vertica/bin/vsql -h 10.161.56.241 Valhalla f3b8i2z_liu_fd -wchangeme \
-F $'^' -At -o $OUTPUT \
-c "
    select t.merchant_id,  t.platform_id, t.tkt as ticket
    from
        (SELECT  merchant_id,  platform_id, sum(txn_amt) as total,  avg(txn_amt_avg)  as tkt FROM fletcher_prod.txns_agg_by_mid_mdate_unfiltered
        where merchant_txn_date between '2018-01-01' and '2018-12-31'
        group by 1,2 ) t
    where   t.total<10000000
    group by 1,2,3;

"
wc -l $OUTPUT
gzip $OUTPUT