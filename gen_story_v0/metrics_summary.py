import logging.config
import os
import os.path

import pandas as pd
import psycopg2

import Config

logging.config.fileConfig('config/log_config.ini')
LOG = logging.getLogger('story_generator_app')

class MetricsSummary:
    # get metrics connection
    def __init__(self, config, mid=1):
        self.config = config
        self.mid = mid
        metrics_conn_info = {'host': config['metrics_host'],
                             'port': config['metrics_port'],
                             'user': config['metrics_user'],
                             'password': config['metrics_password'],
                             'dbname': config['metrics_db_name']}
        self.con_pgres = psycopg2.connect(**metrics_conn_info)
        self.con_pgres.autocommit = False

    # get next mid
    def next_task(self):
        return 'task'

    # create sql and parms -- should have a list of parms and sqls
    def sql_parms(self, task):
        parms = {'mid': self.mid,
                 'time_window': 'daily'}

        sql = "SELECT platform_id, merchant_id, time_window, payment_count, revenue, date, customer_class " \
              "FROM sbp_registered.metrics " \
              "WHERE merchant_id=%s and time_window='%s'" % (self.mid, 'daily')
        x_sql = sql.format(**parms)
        LOG.info("parms = %s", parms)
        LOG.info("sql = %s", x_sql)
        return x_sql, parms

    def sqlDF(self, sql):
        curs = self.con_pgres.cursor()
        # execute and get DF
        curs.execute(sql)
        rows = curs.fetchall()
        if len(rows) > 0:
            df = pd.DataFrame(rows)
            df.columns = ['platform_id', 'merchant_id', 'time_window',
                          'payment_count', 'revenue', 'date', 'customer_class']
            total_revenue = float(sum(df['revenue']))
            total_count = float(sum(df['payment_count']))
            return 'df'

    # create story
    def create_story(self, df):
        return 'story'

    def generate_loop(self):
        task = self.next_task()

        sql, parms = self.sql_parms(task)

        df = self.sqlDF(sql)

        story = self.create_story(df)

    def close(self):
        if self.con_pgres is not None:
            self.con_pgres.close()

def main():
    # get config
    config_filename = 'config/story_generator_config.ini'
    LOG.info("config file name = %s", config_filename)
    config = {}
    if os.path.isfile(config_filename):
        config = Config.load_config(config_filename)
    else:
        LOG.info("Config file not found")
        exit(-1)

    metricsGenerator = MetricsSummary(config)

    # loop via mid and do daily/weekly/weekly finds
    metricsGenerator.generate_loop()
    metricsGenerator.close()

if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        LOG.error('Failed with story_generator', exc_info=True)
