SELECT   a.merchant_id, 
         Hash(a.merchant_id) AS hashedcohortmid, 
         a.platform_id, 
         merchant_name, 
         a.ticket, 
         a.revenue, 
         merchant_mcc, 
         address_dba_lat, 
         address_dba_lon, 
         Cast(b.distance_in_mi AS FLOAT),
         'mcc' AS  cohort_source
FROM     ( 
         ( 
                  SELECT   merchant_id, 
                           platform_id, 
                           Cast(Sum(settle_txn_amt) AS FLOAT)                         AS revenue,
                           Cast ( Sum(settle_txn_amt)/Sum(settle_txn_count) AS FLOAT )AS ticket
                  FROM     fletcher_prod.txns_agg_by_mid_mdate_unfiltered 
                  WHERE    merchant_txn_date>= '{txn_start_date}' 
                  AND      merchant_txn_date< '{txn_end_date}' 
                  AND      settle_txn_amt > 0 
                  GROUP BY merchant_id, 
                           platform_id ) a 
JOIN 
         ( 
                  SELECT   merchant_id, 
                           platform_id, 
                           merchant_name, 
                           address_dba_lat, 
                           address_dba_lon, 
                           merchant_mcc, 
                           pgrm_5003_fld_0001_action, 
                           pgrm_5003_fld_0001_val, 
                           pref_anly_opt_out_cd, 
                           bank_data_right_usage, 
                           111.045*0.621* Degrees(Acos(Cos(Radians(latpoint)) * Cos(Radians(address_dba_lat)) * Cos(Radians(longpoint) - Radians(address_dba_lon)) + Sin(Radians(latpoint)) * Sin(Radians(address_dba_lat))))
                                AS distance_in_mi
                  FROM     fletcher_prod.mdm_merchants_snapshot
                  JOIN 
                           ( 
                                  SELECT {latpoint}  AS latpoint, 
                                         {longpoint} AS longpoint ) AS p 
                  ON       TRUE
                  WHERE    address_dba_lat BETWEEN {latpoint}-{lat_adjust} AND      {latpoint}+{lat_adjust}
                  AND      address_dba_lon BETWEEN {longpoint}-{lon_adjust} AND      {longpoint}+{lon_adjust}
                  AND      ( ( 
                                             platform_id IN (1000,1010,1020) 
                                    AND      pgrm_5003_fld_0001_action = 'A'::varchar(1) 
                                    AND      pgrm_5003_fld_0001_val IN ('2')) 
                           OR       ( 
                                             platform_id = 1030 
                                    AND      pref_anly_opt_out_cd IN ('2', 
                                                                      '3')) 
                           OR       ( 
                                             platform_id = 1040 
                                    AND      bank_data_right_usage IN ('2'))
                           OR ( platform_id !=1090 )
                           OR  marker IN ('891','049','124','127','188','202','216','475','481','516',
                                            '610','614','890','894','930','932','938','O57','OP4',
                                            '225','226','922','924','257','107')
                            )
                  {exclude_merchants}
                  {cohort_mccs}
                  ORDER BY distance_in_mi ASC ) b 
ON       a.merchant_id = b.merchant_id 
AND      a.platform_id = b.platform_id 
         )
WHERE    a.ticket>= {min_ticket_size} 
AND      a.ticket<= {max_ticket_size} 
AND      ( 
                  a.revenue/a.ticket > 100 
         OR       a.revenue > 10000.0)
ORDER BY b.distance_in_mi ASC,
         a.revenue, 
         a.merchant_id limit {cohort_limit};