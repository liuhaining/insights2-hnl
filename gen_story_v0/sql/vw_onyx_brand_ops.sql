DROP VIEW  "public"."vw_onyx_brand_ops";
CREATE VIEW "public"."vw_onyx_brand_ops" AS
SELECT i.id,i.merchant_name,i.uid,i.mid,i.mcc,i.brand_name,i.pid,
i.cohort_mccs,i.street,i.city,i.state,i.zip,i.store_id,
i.exclude_cohort_names,i.max_ticket_size,i.min_ticket_size,i.max_distance,
i.factual_ids,i.lat,i.lon,i.cohort_size,o.month_info,o.status,o.algorithm,o.comment,o.is_mid_active,o.use_ticket_range,o.ticket,o.rev_cht_tgt,o.tkt_cht_tgt
FROM brand_info i 
LEFT JOIN 
( 
   SELECT * FROM (
         SELECT uid,mid,pid,month_info,status,algorithm,comment,is_mid_active,use_ticket_range,ticket,rev_cht_tgt,tkt_cht_tgt,ROW_NUMBER () OVER ( PARTITION BY uid,mid,pid ORDER BY uid,mid,pid,modified_on DESC ) AS row_no
         FROM brand_op
         ) AS a 
    WHERE a.row_no = 1
) AS o
ON (i.uid=o.uid) AND 
(i.mid=o.mid) AND 
(i.pid=i.pid) 
;

/*
truncate table brand_info;

insert into brand_info
 select   id, merchant_name, uid, mid, mcc, brand_name, pid, cohort_mccs, street, '' as city, '' as state, zip,
          store_id, exclude_cohort_names, max_ticket_size, min_ticket_size, max_distance, factual_ids,
           modified_on , lat, lon, cohort_size
from brand_to_be_deleted 
where month_info = 201708;

*/
